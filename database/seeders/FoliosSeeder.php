<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class FoliosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //----------------------------------------------------------------//
       $folio =  \App\Models\Folio::create([
            'codigo' => Str::random(12),
            'corrida' => '1234567',
            'participante_id' => 1,
            'created_at' => now(),
        ]);

        \App\Models\Premio::create([
            'folio_codigo' => $folio->codigo,
            'codigo' => 'LOREMP30',
            'premio_tipo_id' => 1,
        ]);

        //----------------------------------------------------------------//

        $folio =  \App\Models\Folio::create([
            'codigo' => Str::random(12),
            'corrida' => '1234567',
            'participante_id' => 2,
            'created_at' => now(),
        ]);

        \App\Models\Premio::create([
            'folio_codigo' => $folio->codigo,
            'codigo' => 'LOREMP30',
            'premio_tipo_id' => 1,
        ]);
        //----------------------------------------------------------------//


        $folio = \App\Models\Folio::create([
            'codigo' => Str::random(12),
            'corrida' => '1234567',
            'participante_id' => 2,
            'created_at' => now(),
        ]);

        \App\Models\Premio::create([
            'folio_codigo' => $folio->codigo,
            'codigo' => 'LOREMP30',
            'premio_tipo_id' => 1,
        ]);
        //----------------------------------------------------------------//


        $folio =  \App\Models\Folio::create([
            'codigo' => Str::random(12),
            'corrida' => '1234567',
            'participante_id' => 3,
            'created_at' => now(),
        ]);

        \App\Models\Premio::create([
            'folio_codigo' => $folio->codigo,
            'codigo' => 'LOREMP30',
            'premio_tipo_id' => 1,
        ]);
        //----------------------------------------------------------------//

    }
}
