<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PremiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Models\PremioTipo::create([
            'clase' => 'desc10',
            'imagen' => 'https://google.com/',
            'titulo' => '10% de descuento',

        ]);

        \App\Models\PremioTipo::create([
            'clase' => 'desc15',
            'imagen' => 'https://google.com/',
            'titulo' => '15% de descuento',

        ]);

        \App\Models\PremioTipo::create([
            'clase' => 'balon',
            'imagen' => 'https://google.com/',
            'titulo' => 'Balon',

        ]);

        \App\Models\PremioTipo::create([
            'clase' => 'playera',
            'imagen' => 'https://google.com/',
            'titulo' => 'Playera ',

        ]);

    }
}
