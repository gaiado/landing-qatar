<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("settings")->insert([
            [
                "name" => "premio_balon_count",
                "value" => "1",
            ],
            [
                "name" => "premio_playera_count",
                "value" => "1",
            ],
            [
                "name" => "premio_desc_10_count",
                "value" => "1",
            ],
            [
                "name" => "premio_desc_15_count",
                "value" => "1",
            ],
        ]);
    }
}
