<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //----------------------------------------------------------------//
        \App\Models\User::factory()->create([
            'name' => 'Ricardo Antonio',
            'email' => 'ricardo@example.com',
            'password' => Hash::make('password'),

        ])->participante()->create([
            'apellidos' => 'Cruz Cruz',
            'cp' => '77516'
        ]);
        //----------------------------------------------------------------//
        \App\Models\User::factory()->create([
            'name' => 'Pedro Delfino',
            'email' => 'pedrode@example.com',
            'password' => Hash::make('password'),

        ])->participante()->create([
            'apellidos' => 'Mendez Gaona',
            'cp' => '77500'
        ]);

        //----------------------------------------------------------------//

        \App\Models\User::factory()->create([
            'name' => 'Ruben alejandro',
            'email' => 'rubale@example.com',
            'password' => Hash::make('password'),

        ])->participante()->create([
            'apellidos' => 'Poot Peña',
            'cp' => '77300'
        ]);

        //----------------------------------------------------------------//
    }
}
