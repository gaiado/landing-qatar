<?php

namespace App\Values;

use FolioError;
use FolioSuccess;

class FolioValidacionStatusValues
{
    const STRATEGY = [
        'success' => FolioSuccess::class,
        'error' => FolioError::class,
    ];
}
