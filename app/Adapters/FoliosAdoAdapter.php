<?php

namespace App\Adapters;

use App\Contracts\FoliosAdoService;
use Illuminate\Support\Facades\Http;

class FoliosAdoAdapter implements FoliosAdoService
{
    private $endpoint;
    public function __construct()
    {
        $this->endpoint = "https://wsebusiness.ado.com.mx/WSCHOL/";
    }

    public function ConsultarFolio(string $folio)
    {
        //revisar lo del certificado
        $response = Http::withoutVerifying()->post(
            "{$this->endpoint}ConsultarFolio",
            [
                "numeFolio" => $folio,
                "mac" => "00-50-56-A7-4E-8F",
                "usuarioSeguridad" => "WS_HOLA",
            ]
        );

        return $response->json();
    }
}
