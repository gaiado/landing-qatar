<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use DateTime;

class DateValidation implements Rule
{
  /**
   * Create a new rule instance.
   *
   * @return void
   */
  public $dateBefore = "";
  public $dateAfter = "";


  public function __construct(string $dateBefore, string $dateAfter)
  {
    $this->dateBefore = $dateBefore;
    $this->dateAfter = $dateAfter;
  }
  /**
   * Determine if the validation rule passes.
   *
   * @param  string  $attribute
   * @param  mixed  $value
   * @return bool
   */
  public function passes($attribute, $value)
  {
    #Valor obtenido de la API
    $date = strtotime(str_replace("/", "-", $value));

    #valores asignados del env
    $datebefore = strtotime(str_replace("/", "-", $this->dateBefore));

    $dateafter = strtotime(str_replace("/", "-", $this->dateAfter));

    return $date < $datebefore && $date > $dateafter;
  }

  /**
   * Get the validation error message.
   *
   * @return string
   */
  public function message()
  {
      return "La fecha de compra no se encuentra dentro de la promoción";
  }
}
