<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use DateTime;

#test

class TravelValidation implements Rule
{
  /**
   * Determine if the validation rule passes.
   *
   * @param  string  $attribute
   * @param  mixed  $value
   * @return bool
   */
  public function passes($attribute, $value)
  {
    #rangos de fecha de promoción valida
    $datebefore = strtotime(str_replace("/", "-", config('dates.date_before')));
    $dateafter = strtotime(str_replace("/", "-",  config('dates.date_after')));

    #hora y fecha actual
    $now = strtotime('now');
    #valor dado por la api
    $date = strtotime(str_replace("/", "-", $value));


    //el viaje se encuentra dentro del rango de la promocion
    if($date < $datebefore && $date > $dateafter){
        return $date < $now; //el viaje ya ha sido realizado
    }

    return false;
  }

  /**
   * Get the validation error message.
   *
   * @return string
   */
  public function message()
  {
    return "La fecha del viaje no se encuentra dentro de la promoción";
  }
}
