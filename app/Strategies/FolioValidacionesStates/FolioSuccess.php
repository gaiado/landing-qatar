<?php

use App\Strategies\FolioValidacionService;
use Symfony\Component\HttpFoundation\Request;

class FolioSuccess implements FolioValidacionService
{
   public function getValidationStates(Request $request)
    {
        return "success 200 ok";
    }
}
