<?php

use App\Strategies\FolioValidacionService;
use Symfony\Component\HttpFoundation\Request;

class FolioError implements FolioValidacionService
{
   public function getValidationStates(Request $request)
    {
        return "Error 404";
    }
}
