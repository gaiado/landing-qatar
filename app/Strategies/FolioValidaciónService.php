<?php

namespace App\Strategies;
use Illuminate\Http\Request;

interface FolioValidacionService
{
    public function getValidationStates(Request $request);
}
