<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class ReenviarCorreosVerificacion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correos-verificacion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::whereNull('email_verified_at')
        ->get();
   
        foreach ($users as $user) {
            $user->sendEmailVerificationNotification();
        }
        
        return 0;
    }
}
