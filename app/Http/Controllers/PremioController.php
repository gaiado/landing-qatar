<?php

namespace App\Http\Controllers;

use App\Adapters\FoliosAdoAdapter;
use App\Models\Folio;
use App\Models\Participante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Rules\DateValidation;
use App\Rules\FirstValidation;
use App\Rules\TravelValidation;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\NotIn;
use App\Models\Premio;
use App\Mail\PremioAsignado;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class PremioController extends Controller
{
    private $foliosAdoAdapter;
    private $messages = [
        "ItemBoleto.corridaId.in" => "Número de corrida no válido",
        "ItemBoleto.descMarca.in" =>
            "La marca del boleto no entra en la promoción",
        "ItemBoleto.statusBoleto.in" => "Estatus del boleto no válido",
        "ItemBoleto.descMotivoCancelacion.in" => "Estatus del boleto no válido",
    ];

    public function __construct(FoliosAdoAdapter $foliosAdoAdapter)
    {
        $this->foliosAdoAdapter = $foliosAdoAdapter;
    }

    protected function validator(array $request)
    {
        return Validator::make($request, [
            "folio" => [
                "bail",
                "required",
                "numeric",
                "unique:folios,codigo",
                "max_digits:12",
                "min_digits:6",
            ],
            "corrida" => [
                "bail",
                "required",
                "numeric",
                //"max_digits:7",
                //"min_digits:5",
            ],
        ]);
    }

    protected function validatorFolio($response, $request)
    {
        $corrida = $request->get("corrida");

        return Validator::make(
            $response,
            [
                "descError" => ["max:0"],
                "ItemBoleto.corridaId" => [
                    "bail",
                    "required",
                    "in:" . $corrida,
                    //"max:99",
                    //"min:5",
                ],
                "ItemBoleto.statusBoleto" => ["bail", Rule::in(["V","C"])],
                "ItemBoleto.descMotivoCancelacion" => ["bail", Rule::in(["TRANSFERENCIA",""])],
                "ItemBoleto.fecHorViaje" => ["bail", new TravelValidation()],
                "ItemBoleto.fecHorVta" => [
                    "bail",
                    new DateValidation(
                        config("dates.date_before"),
                        config("dates.date_after")
                    ),
                ],
                "ItemBoleto.descMarca" => [
                    "bail",
                    "required",
                    Rule::in([
                        "ADO",
                        "ADO PLATINO",
                        "ADO GL",
                        "PLUSS",
                        "RIVIERA",
                        "DIAMANTE",
                        "PRIMERA EDO",
                        "AU",
                        "OCC",
                    ]),
                ],
                #'ItemBoleto.descPuntoVenta' => ['NotIn:996 Clickbus,894 Internet Mi Escape,'],
            ],
            $this->messages
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if (isset($user) && is_null($user->email_verified_at)) {
            return redirect('/email/verify');
        }
        return view("premios.formulario");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validamos inputs de folio y la corrida (junta al captcha)
        $this->validator($request->all())->validate();
        #obtenemos el folio
        $folio = $request->get("folio");
        #consultamos la api de HOLA
        $response = $this->foliosAdoAdapter->ConsultarFolio($folio);
        #validaciones especificas del folio
        $this->validatorFolio($response, $request)->validate();

        //insercion de folio en database
        Folio::create([
            "codigo" => $folio,
            "corrida" => $request->get("corrida"),
            "participante_id" => $request->user()->id,
            "created_at" => date("Y-m-d H:i:s"),
            "fecha_venta" => $response["ItemBoleto"]["fecHorVta"],
            "fecha_viaje" => $response["ItemBoleto"]["fecHorViaje"],
        ]);

        //obtencion de premio
        $premio = Premio::siguiente()->first();
        //asignacion de premio al folio
        $premio->update(["folio_codigo" => $folio]);

        //enviar correo
        Mail::to(Auth::user()->email)->queue(new PremioAsignado($premio->id));
        //renderizar vista de premio
        return view("premios.canje");
    }
}
