<?php

namespace App\Http\Controllers;

use App\Models\Folio;
use App\Mail\PremioAsignado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    public function centros()
    {
        return view("page.centros");
    }

    public function preguntas()
    {
        return view("page.preguntas");
    }

    public function mecanica()
    {
        $ultimo = Folio::with('premio')->ultimo()->first();
        return view("page.mecanica", compact("ultimo"));
    }

    public function bases()
    {
        return view("page.bases");
    }
    public function misPremios()
    {
        $user = Auth::user();
        $premios = $user->premios()->get();
        return view("page.mispremios",compact('premios'));
    }

    public function finalizado()
    {
        $ultimo = Folio::with('premio')->ultimo()->first();
        return view("finalizado", compact("ultimo"));
    }
}
