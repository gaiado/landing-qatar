<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Premio extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = "premios";
    protected $fillable = ["folio_codigo"];
    protected $hidden = ["codigo", "folio_codigo"];

    public function scopeSiguiente($query)
    {
        return $query->whereNull("folio_codigo");
    }

    public function folio()
    {
        return $this->belongsTo(Folio::class);
    }
    public function tipo()
    {
        return $this->belongsTo(PremioTipo::class,'premio_tipo_id');
    }
    
}
