<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Folio extends Model
{
    use HasFactory;

    protected $keyType = "string";
    protected $primaryKey = "codigo";
    public $incrementing = false;
    public $timestamps = false;
    protected $hidden = ["codigo", "corrida"];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        "codigo",
        "corrida",
        "fecha_venta",
        "fecha_viaje",
        "participante_id",
        "created_at",
    ];

    public function premio()
    {
        return $this->hasOne(Premio::class, "folio_codigo", "codigo");
    }
    

    public function scopeUltimo($query)
    {
        return $query->orderBy("created_at", "desc");
    }

    public function participante()
    {
        return $this->belongsTo(Participante::class, "id");
    }

    public function getFechaAttribute()
    {
        $date = strtotime($this->created_at);
        $diassemana = [
            "Domingo",
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes",
            "Sábado",
        ];
        $meses = [
            "enero",
            "febrero",
            "marzo",
            "abril",
            "mayo",
            "junio",
            "julio",
            "agosto",
            "aeptiembre",
            "octubre",
            "noviembre",
            "diciembre",
        ];

        return $diassemana[date("w", $date)] .
            " " .
            date("d", $date) .
            " de " .
            $meses[date("n", $date) - 1] .
            " de " .
            date("Y - H:i", $date);
    }
}
