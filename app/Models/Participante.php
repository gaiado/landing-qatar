<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Participante extends Model
{
    use HasFactory;

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'apellidos',
        'cp'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function folios()
    {
        return $this->hasMany(Folio::class,'participante_id');
    }
}
