<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PremioTipo extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = "premio_tipos";
    protected $fillable = ["disponible"];

    public function scopeDisponible($query)
    {
        $query->where("disponible", ">", 0);
    }

}
