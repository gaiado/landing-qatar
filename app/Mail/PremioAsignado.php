<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Premio;

class PremioAsignado extends Mailable
{
    use Queueable, SerializesModels;
    private $premio_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($premio_id)
    {
        $this->premio_id = $premio_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $premio = Premio::find($this->premio_id);
        
        return $this->view("mails.premio-asignado-$premio->premio_tipo_id", compact("premio"));
    }
}
