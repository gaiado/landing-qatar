<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Auth\Notifications\VerifyEmail;

class VerifyEmailQueued extends VerifyEmail implements ShouldQueue
{
    use Queueable;

     /**
     * Get the verify email notification mail message for the given URL.
     *
     * @param  string  $url
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    protected function buildMailMessage($url)
    {
        return (new MailMessage)
            ->subject('Confirme su dirección de correo electrónico')
            ->greeting('Hola')
            ->line('Haga clic en el botón de abajo para verificar su dirección de correo electrónico.')
            ->action('Confirme su dirección de correo electrónico', $url)
            ->line('Si no creó una cuenta, no se requiere ninguna otra acción.')
            ->salutation('Saludos, '. config('app.name'));
    }
}
