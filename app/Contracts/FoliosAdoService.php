<?php

namespace App\Contracts;

interface FoliosAdoService
{
    public function consultarFolio(string $folio);
}