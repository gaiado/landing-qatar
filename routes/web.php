<?php

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\PageController::class, 'finalizado']);

/*Route::get("/", function () {
    return view("welcome");
});

Auth::routes(['verify' => true]);
*/
Route::controller(App\Http\Controllers\PageController::class)->group(
    function () {
        //Route::get("/home", "home")->name("home");
        Route::get("/centros-de-canje", "centros")->name("centros");
        //Route::get("/preguntas-frecuentes", "preguntas")->name("preguntas");
        //Route::get("/mecanica", "mecanica")->name("mecanica");
        Route::get("/bases", "bases")->name("bases");
        //Route::get("/mis-premios", "misPremios")->name("mispremios")->middleware(['auth', 'verified']);
    }
);
/*
Route::controller(App\Http\Controllers\PremioController::class)->group(
    function () {
        Route::get("/premios", "create")->name("premios");
        Route::post("/premios", "store")->name("premios")->middleware(['auth', 'verified']);
    }
);*/
