@extends('layouts.app')
@section('title')
Iniciar sesión
@endsection
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">
@section('content')
<div class="registro">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 col-sm-9 col-xl-5">
                <div class="formulario">
                    <div class="text-center mt-4 pt-4">
                        <p class="mb-4">
                            ¡Bienvenido al sitio oficial de la promoción Súbete a la emoción de Qatar!

                        </p>
                        <p class="mb-4">
                            A continuación ingresa tus datos:
                        </p>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="mb-3">
                            <label for="email" class="">{{ __('Correo electrónico') }}</label>

                            <div class="">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="password" class="">Contraseña</label>

                            <div id="cpassword" class="">                                    
                                <div class="input-group mb-3">
                                    <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">
                                    <button onclick="showPass()" class="btn btn-secondary btn-show" type="button"><i class="fas fa-eye"></i></button>
                                    <button onclick="hidePass()" class="btn btn-secondary btn-hide" type="button"><i class="fas fa-eye-slash"></i></i></button>
                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="text-center">
                            <div class="mb-4">
                                @if (Route::has('password.request'))
                                <a class="btn btn-link text-white" href="{{ route('password.request') }}">
                                    ¿Olvidaste tu contraseña?
                                </a>
                                @endif
                            </div>
                        </div>
                        <div class="mb-4">
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary px-4">
                                    INICIAR SESIÓN
                                </button>
                            </div>
                        </div>
                        <div class="text-center">
                            <div class="mb-4">
                                <p class="text-white">
                                    ¿Deseas registrarte? <a class="text-white" href="{{ route('register') }}"> Haz click aquí</a>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<component :is="'script'">
    function showPass() {
        var elemento = document.getElementById("cpassword");
        document.getElementById("password").setAttribute("type", "text");;
        elemento.className = 'show-password';
    }
    function hidePass() {
        var elemento = document.getElementById("cpassword");
        document.getElementById("password").setAttribute("type", "password");
        elemento.className = '';
    }
</component>
@endsection