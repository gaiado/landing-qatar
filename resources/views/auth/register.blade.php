@extends('layouts.app')
@section('title')
Regístrate
@endsection
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">
@section('content')
<div class="registro">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 col-sm-9 col-xl-5">
                <div class="formulario">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('register') }}">
                        @csrf

                        <div class="mb-3">
                            <label for="name" class="">{{ __('Nombre(s)')
                                }}</label>

                            <div class="">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="lastname" class="">{{ __('Apellidos')
                                }}</label>

                            <div class="">
                                <input id="lastname" type="text"
                                    class="form-control @error('lastname') is-invalid @enderror" name="lastname"
                                    value="{{ old('lastname') }}" required>
                                @error('lastname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="postalcode" class="">{{ __('Código
                                Postal') }}</label>

                            <div class="">
                                <input id="postalcode" maxlength="5"
                                    class="form-control @error('postalcode') is-invalid @enderror" name="postalcode"
                                    value="{{ old('postalcode') }}" required>
                                @error('postalcode')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="">Correo electrónico</label>

                            <div class="">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="email_confirmation" class="">Repetir correo electrónico</label>

                            <div class="">
                                <input id="email_confirmation" type="email" class="form-control @error('email_confirmation') is-invalid @enderror"
                                    name="email_confirmation" required autocomplete="email_confirmation">

                                @error('email_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="password" class="">Contraseña</label>

                            <div id="cpassword" class="">                                    
                                <div class="input-group mb-3">
                                    <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">
                                    <button onclick="showPass()" class="btn btn-secondary btn-show" type="button"><i class="fas fa-eye"></i></button>
                                    <button onclick="hidePass()" class="btn btn-secondary btn-hide" type="button"><i class="fas fa-eye-slash"></i></i></button>
                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="password-confirm" class="">Repetir contraseña</label>
                            <div id="cpassword2">
                                <div class="input-group mb-3">
                                    <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                                    <button onclick="showPass2()" class="btn btn-secondary btn-show" type="button"><i class="fas fa-eye"></i></button>
                                    <button onclick="hidePass2()" class="btn btn-secondary btn-hide" type="button"><i class="fas fa-eye-slash"></i></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label"></label>
                            <div class="">
                                <captcha></captcha>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="captcha" class="">Captcha</label>
                            <div class="">
                                <input id="captcha" type="text"
                                    class="form-control @error('password') is-invalid @enderror" name="captcha"
                                    required>
                                @error('captcha')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="">
                            <div class="text-center mt-4 pt-2">
                                <button type="submit" class="btn btn-primary px-4">
                                    REGISTRARME
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<component :is="'script'">
    function showPass() {
        var elemento = document.getElementById("cpassword");
        document.getElementById("password").setAttribute("type", "text");;
        elemento.className = 'show-password';
    }
    function hidePass() {
        var elemento = document.getElementById("cpassword");
        document.getElementById("password").setAttribute("type", "password");
        elemento.className = '';
    }
    function showPass2() {
        var elemento = document.getElementById("cpassword2");
        document.getElementById("password-confirm").setAttribute("type", "text");;
        elemento.className = 'show-password';
    }
    function hidePass2() {
        var elemento = document.getElementById("cpassword2");
        document.getElementById("password-confirm").setAttribute("type", "password");
        elemento.className = '';
    }

    document.addEventListener("contextmenu", function(event){
        event.preventDefault();
    }, false);

    document.addEventListener("copy", function(event){
        event.clipboardData.setData("text/plain", "");
        event.preventDefault();
    }, false);
</component>
@endsection