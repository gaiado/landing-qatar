@extends('layouts.passwords')
@section('title')
Verificar correo electrónico
@endsection
@section('content-passwords')
<div class="card">
                <div class="card-header">Verifique su correo electrónico</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            Un nuevo enlace de verificación ha sido enviado a su correo electrónico
                        </div>
                    @endif

                    <p>Antes de proseguir, busque en su correo electrónico un link de verificación.</p>
                    <p> Correo electrónico registrado: <code>    {{auth()->user()->email}}</code></p>
                    Si no recibió el email de verificación de
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">clic aquí para solicitar un nuevo enlace</button>.
                    </form>
                </div>
            </div>
@endsection
