@extends('layouts.premios')

@section('content-premios')
<div class="card p-4 pb-2 pt-3" id="formulario-premio">
    <div class="ticket"  onclick="hideTicket()"></div>
    <div class="card-body">
        <h3 class="mb-3">Reclama tu recompensa</h3>
        @if ($errors->any())
        <div class="alert alert-danger">
            {{ $errors->all()[0] }}
        </div>
        @endif
        <form method="POST" action="{{ route('premios') }}">
            @csrf
            <div class="mb-3">
                <div class="label-item">
                    <button type="button" onclick="showTicket1()">?</button>
                    <label for="folio" class="col-form-label"> Folio del boleto
                        adquirido.</label>
                </div>
                <div>
                    <input id="folio" type="text" placeholder="Ingresa tu número de folio del boleto adquirido."
                        class="form-control @error('folio') is-invalid @enderror" name="folio"
                        value="{{ old('folio') }}" required>
                </div>
            </div>
            <div class="mb-4">
                <div class="label-item">
                    <label for="corrida" class="col-form-label">Número de corrida del
                        boleto adquirido. </label>
                    <button type="button" onclick="showTicket2()">?</button>
                </div>
                <div>
                    <input id="corrida" type="text" placeholder="Ingresa tu número de corrida del boleto adquirido."
                        class="form-control @error('corrida') is-invalid @enderror" name="corrida"
                        value="{{ old('corrida') }}" required>
                </div>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" required value="" id="flexCheckDefault">
                <label class="form-check-label" for="flexCheckDefault">
                    Estoy de acuerdo con <a href="#">Términos y Condiciones</a> así como el <a href="#"> Aviso de
                        Privacidad</a>
                </label>
            </div>
            <div class="form-check mb-4">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                <label class="form-check-label" for="flexCheckChecked">
                    Deseo recibir promociones exclusivas
                </label>
            </div>
            <div class="row justify-content-center">
                <div class="col-auto">
                    <button type="submit" class="btn btn-light px-4">
                        CONTINUAR
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<component :is="'script'">
    function showTicket1() {
        var elemento = document.getElementById("formulario-premio");
        elemento.className = 'card p-4 pb-2 pt-3 show-ticket-1';
    }
    function showTicket2() {
        var elemento = document.getElementById("formulario-premio");
        elemento.className = 'card p-4 pb-2 pt-3 show-ticket-2';
    }

    function hideTicket() {
        var elemento = document.getElementById("formulario-premio");
        elemento.className = 'card p-4 pb-2 pt-3';
    }
</component>
@endsection