@extends('layouts.premios')

<script src="{{asset('js/confetti.js')}}"></script>

@section('content-premios')
<div class="canje text-center" id="canje">
    <div class="bg">
        <canvas id="confetti"></canvas>
    </div>
    <h2>¡TU PREMIO VA EN CAMINO!</h2>
    <img class="pb-1 img-fluid" onload="iniciarConfetti();" src="https://www.ado.com.mx/images/ado.svg" alt="ADO">
    <p class="text-secondary">Estamos validando tu numero de folio...</p>
    <img class="img-flid" onload="iniciarConfetti();" src="{{asset('img/camion.png')}}" alt="autobus">
    <div class="loading"></div>
</div>

<div class="canje text-center" id="premio" style="display: none">
    <h2>Gracias por participar</h2>
    <img class="img-flid" src="{{asset('img/premios.png')}}" alt="premios">
    <p class="text-secondary">Te agradecemos por haber participado, ahora revisa tu correo y confirma tu premio.</p>
    <a href="{{ route('premios') }}" class="btn btn-warning">
        INGRESAR OTRO FOLIO
    </a>
</div>

@endsection