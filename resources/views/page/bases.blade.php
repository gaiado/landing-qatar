@extends('layouts.app')

@section('content')
<div class="bases">
    <div class="wrapper pt-4">
        <div class="container pb-4 mb-4">
            <h2 class="titulo-balon">Bases</h2>
            <balon></balon>

            <div class="contenido-bases mt-4">
                <div class="text-center">
                    <h6 class="mb-0">BASES Y CONDICIONES GENERALES DE LA PROMOCIÓN</h6>
                    <h6>“SÚBETE A LA EMOCIÓN DE QATAR”</h6>
                </div>
                <p>
                    Para participar se deberá dar lectura integra de los siguientes términos, bases y
                    condiciones (en adelante las “Bases”), además de cumplir totalmente con los requisitos
                    aquí establecidos, lo cual implica su total comprensión y aceptación.
                </p>
                <div class="text-justify">
                    <ol class="ps-3">
                        <li>
                            </p>NOMBRE DE LA PROMOCIÓN: “SÚBETE A LA EMOCIÓN DE QATAR” (en lo sucesivo referida como “la
                            Promoción”).</p>
                        </li>
                        <li>
                            </p>NOMBRE Y DOMICILIO DEL RESPONSABLE DE LA PROMOCIÓN: ADO Y EMPRESAS COORDINADAS S.A. DE
                            C.V., con domicilio en CALZADA IGNACIO ZARAGOZA 200 EDIF B 2DO PISO COLONIA 7 DE JULIO,
                            VENUSTIANO CARRANZA C.P. 15390.</p>
                        </li>
                        <li>
                            </p>VIGENCIA DE LA PROMOCIÓN: El periodo de participación de la mecánica de “la Promoción”
                            inicia a las 00:01:00 horas (hora (GMT-5) Hora en la Ciudad de México, CDMX) del día 04 de
                            octubre de 2022 y concluye a las 23:59:00 (hora (GMT-5) hora en la Ciudad de México, CDMX)
                            del 15 de diciembre de 2022 y/o hasta agotar existencias de los incentivos ofrecidos.</p>
                        </li>
                        <li>
                            </p>COBERTURA GEOGRÁFICA: Ciudad de México, Estado de México, Tamaulipas, Veracruz, Tabasco,
                            Campeche, Quintana Roo, Yucatán, Puebla, Chiapas, Oaxaca, Guerrero, Morelos, Tlaxcala e
                            Hidalgo.</p>
                        </li>
                        <li>
                            </p>TELÉFONO PARA INFORMACIÓN Y ACLARACIONES: Estará disponible el correo electrónico
                            solucioneshola@ado.com.mx o bien podrá enviarse un mensaje de Whatsapp al: 5543864652 con
                            horario de atención lunes a sábado de 6:00 a 23:00 hrs domingo y días festivos de 7:00 a 22:00 hrs.
                            </p>
                        </li>
                        <li>
                            </p>REQUISITOS PARA PARTICIPAR: Podrá participar el público en general que desee participar
                            en la “PROMOCIÓN” y que reúna todos y cada uno de los siguientes requisitos: i) Ser mayor de
                            18 años, cumplidos al inicio de la vigencia de la presente “PROMOCIÓN” ii) Contar con una
                            identificación oficial vigente tales como: INE o IFE, pasaporte, cartilla del servicio
                            militar liberada, cédula profesional, forma migratoria (FM2 o FM3) vigente que acredite la
                            legal residencia en el país del “PARTICIPANTE”. iii) Contar con R.F.C. con homoclave.
                            Dicha documentación se le solicitará exhibirla
                            únicamente a quienes resulten ganadores de alguno de los incentivos ofrecidos, a efecto de
                            acreditar su identidad. iv) Realizar el registro individual como “PARTICIPANTE”,
                            proporcionando datos verídicos, v) Haber viajado en autobuses de cualquiera de las marcas
                            participantes así como no haber cancelado dicho boleto.</p>
                        </li>
                        <li>
                            </p>ESTABLECIMIENTOS PARTICIPANTES: Para efectos de el concurso, no existen establecimientos
                            físicos participantes en donde se realice la promoción, en virtud de que la participación se
                            efectúa través de la página web: https://www.elautobusmundialista.com.mx No obstante, participan
                            todas las compras que cumplan con los requisitos del punto 6 efectuadas en alguno de los
                            canales de compra mencionados en el punto 9.
                            </p>
                        </li>
                        <li>
                            </p>MARCAS Y RUTAS PARTICIPANTES:ADO, ADO AEROPUERTO (Excepto vehículos pequeños tipo
                            Sprinter) ADO GL, ADO PLATINO, OCC, ESTRELLA DE ORO PLUSS, DIAMANTE Y AU (Servicio Directo
                            Económico).
                            </p>
                        </li>
                        <li>
                            </p>CANALES DE COMPRA PARTICIPANTES: Todos los puntos de venta instalados en las terminales,
                            aplicación móvil y sitios de Internet: www.ado.com.mx, www.miescape.mx, www.clickbus.com.mx,
                            que vendan boletos de cualquiera de las marcas participantes: ADO, ADO AEROPUERTO (Excepto
                            vehículos pequeños tipo Sprinter) ADO GL, ADO PLATINO, OCC, ESTRELLA DE ORO, PLUSS, DIAMANTE Y AU
                            (Servicio Directo Económico) en todas las rutas aplicables a las marcas ya mencionadas.</p>
                        </li>
                        <li>
                            </p>TOTAL, DE INCENTIVOS: Se asignarán un total de 5, 121,556 (Cinco millones, ciento veinte
                            un
                            mil quinientos cincuenta y seis mil incentivos). Ver relación anexo A.</p>
                        </li>
                    </ol>

                    <div class="table-responsive">
                        <table class="table table-bordered border-primary">
                            <thead class="table-primary text-center">
                                <tr>
                                    <th>CANTIDAD</th>
                                    <th>DESCRIPCIÓN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1,525,489</td>
                                    <td class="text-justify">1 (un) Descuento (s) de 10% en taquillas..</td>
                                </tr>
                                <tr>
                                    <td class="text-center">3,588,067</td>
                                    <td class="text-justify">1 (un) Descuento (s) de 15% en taquillas.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">5,000</td>
                                    <td class="text-justify">1 (un) Balón #5, Modelo: Tracer, Construcción de 20 paneles
                                        Termo-Laminado, Material sintético Texturizado, Cámara y válvula de butilo para
                                        mayor retención de aire, Dimensiones: 21.96 x 21.96x21.96 / 438 gr. Con
                                        impresión de
                                        logo “Súbete a la emoción de Qatar”</td>
                                </tr>
                                <tr>
                                    <td class="text-center">3,000</td>
                                    <td class="text-justify"> 1 (una) Playera Adidas (oficial de la Selección)
                                        Corte clásico
                                        Cuello redondo acanalado
                                        Tejido de punto doble 100 % poliéster reciclado, Tecnología AEROREADY, Tejido
                                        transpirable que absorbe la humedad. El diseño incorpora un patrón de diamante,
                                        Dobladillo trasero más largo, Escudo de la Selección Nacional de México, tejido
                                        Estampado de Quetzalcóatl en la nuca, Hecho con materiales reciclados, Color del
                                        artículo: Vivid Green / Collegiate Green Número de artículo: HE8847. Con
                                        impresión
                                        de logo ADO.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">Términos del código para canje del 10%</td>
                                    <td class="text-justify">

                                        <p>1 (un) Código de descuento del 10% (Diez por ciento de descuento) que podrá
                                            ser
                                            utilizado para la compra de un boleto de viaje sencillo de autobús en el
                                            periodo
                                            de vigencia de la promoción, en las marcas y rutas participantes.
                                        </p>
                                        <p>Vigencia:
                                            Del 04 de octubre al 15 de diciembre del 2022. Para viajar (periodo de
                                            viaje)
                                            del 04 de octubre al 15 de diciembre del 2022.
                                        </p>
                                        <p> El 10% de descuento aplica para canjear directamente en taquilla.
                                        </p>
                                        <p> El descuento se aplicará de forma proporcional al valor del precio del
                                            boleto de
                                            viaje entero sencillo, en la compra de los boletos en la modalidad de Compra
                                            Anticipada o Por Autobús Pagas Menos o tarifa entera normal.
                                        </p>
                                        <p> El descuento no aplica en la compra de los boletos de INAPAM, NIÑO, FUERZAS
                                            ARMADAS, PASAJERO QUEDADO, ESTUDIANTES, MAESTROS Y BOLETOS ABIERTOS.
                                        </p>
                                        <p> El descuento en la tarifa en los boletos de las modalidades de Compra
                                            Anticipada
                                            o Por Autobús Pagas Menos o tarifa entera normal NO es acumulable con otras
                                            promociones o descuentos.
                                        </p>
                                        <p> Los boletos adquiridos con este descuento NO SON CANCELABLES, pero si son transferibles en fecha y hora dentro del periodo de la promoción, sin poder transferir el boleto a terceros.
                                        </p>
                                        <p> No aplica con las siguientes formas de pago: Orden de servicio, TURISSSTE,
                                            cupón
                                            Kidde, cupón COTEMAR, pases de traslados, pases de vacaciones, cupón
                                            Interjet,
                                            MAYAPASS, cupón (USA) y cupón efectivo.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">Términos del código para canje del 15%</td>
                                    <td class="text-justify">

                                        <p> 1 (un) Código de descuento del 15% (Quince por ciento de descuento) que
                                            podrá
                                            ser utilizado para la compra de un boleto de viaje sencillo de autobús en el
                                            periodo de vigencia de la promoción, en las marcas y rutas participantes.
                                        </p>
                                        <p> Vigencia:
                                            Del 04 de octubre al 15 de diciembre del 2022. para viajar (periodo de
                                            viaje)
                                            del 04 de octubre al 15 de diciembre del 2022.
                                        </p>
                                        <p> El 15% de descuento aplica para canjear en taquillas, para tu próximo viaje
                                            y
                                            disfrutar de tu destino favorito.
                                        </p>
                                        <p> El descuento se aplicará de forma proporcional al valor del precio del
                                            boleto de
                                            viaje entero sencillo en la compra de los boletos en la modalidad de Compra
                                            Anticipada o Por Autobús Pagas Menos o tarifa entera normal.
                                        </p>
                                        <p> El descuento no aplica en la compra de los boletos de INAPAM, NIÑO, FUERZAS
                                            ARMADAS, PASAJERO QUEDADO, ESTUDIANTES, MAESTROS Y BOLETOS ABIERTOS.
                                        </p>
                                        <p> El descuento en la tarifa en los boletos de las modalidades de Compra
                                            Anticipada
                                            o Por Autobús Pagas Menos o tarifa entera normal NO es acumulable con otras
                                            promociones o descuentos.
                                        </p>
                                        <p> Los boletos adquiridos con este descuento NO SON CANCELABLES, pero si son
                                            transferibles dentro del periodo de la promoción.
                                        </p>
                                        <p> No aplica con las siguientes formas de pago: Orden de servicio, TURISSSTE,
                                            cupón
                                            Kidde, cupón COTEMAR,pases de traslados, pases de vacaciones, cupón
                                            Interjet,
                                            MAYAPASS, cupón (USA) y cupón efectivo.
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <ol start="11" class="ps-3">
                        <li>
                            <p>
                                GARANTÍA DE LOS INCENTIVOS. El responsable de la promoción no otorga garantías sobre los
                                incentivos ofrecidos. No obstante, se dejan a salvo aquellas que provea directamente el
                                fabricante de los productos.
                            </p>
                        </li>
                        <li>
                            <p> MECÁNICA DE REGISTRO Y PARTICIPACIÓN:</p>

                            <p>La participación en esta “PROMOCIÓN”, implicará su conocimiento y aceptación de las bases
                                y mecánica aquí señalada.
                                Quién desee participar en la presente “PROMOCIÓN”, deberá adquirir durante la vigencia
                                de la misma un boleto de cualquiera de las marcas participantes para viajar en
                                cualquiera de las rutas aplicables y deberá seguir los siguientes pasos:
                            </p>
                            <ol type="a">
                                <li>
                                    Una vez que el pasajero haya comprado su boleto, y realizado su viaje, podrá
                                    realizar su registro en el “SITIO WEB” www.elautobusmundialista.com.mx durante la
                                    vigencia de la “PROMOCIÓN”.
                                </li>
                                <li>
                                    <p>Para dar inicio a su registro, el “PARTICIPANTE” (Siempre mayor de edad) deberá
                                        aceptar el Aviso de Privacidad respectivo y deberá de proporcionar la siguiente
                                        información:</p>
                                    <p>DATOS DEL PARTICIPANTE PARA REGISTRARSE:
                                    </p>
                                    <p>
                                    <ul>
                                        <li>Nombre completo: Incluyendo apellidos paterno y materno (NO APODOS, NI
                                            SOBRENOMBRES).</li>
                                        <li>Correo electrónico.</li>
                                        <li>Número de folio del boleto adquirido.</li>
                                        <li>Número de corrida del boleto adquirido.</li>
                                        <li>Código postal</li>
                                    </ul>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Antes de registrarse en el “SITIO WEB” www.elautobusmundialista.com.mx el
                                        “PARTICIPANTE” podrá verificar mediante un contador en tiempo real, cuál fue el
                                        último registro y el folio de participación que se asignó, y así estar en
                                        posibilidades de calcular su registro y encuadrar su participación en el
                                        incentivo
                                        por el que desea participar con base a los folios ganadores al incentivo según
                                        la
                                        TABLA DE ASIGNACIÓN DE INCENTIVOS anexo a las presentes bases.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        El total de registros participantes será de 5,121,556 registros, una vez
                                        alcanzado
                                        el registro número 5,121,556 o el término de la vigencia de la “PROMOCIÓN”, se
                                        cerrará el “SITIO WEB” www.elautobusmundialista.com.mx
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Una vez realizado el registro del “PARTICIPANTE”, de manera correcta, el sistema
                                        de
                                        registro verificará que el boleto no se hubiera cancelado y que el viaje se
                                        hubiera
                                        efectuado. Corroborando la información el “PARTICIPANTE” será posible ganador a
                                        uno
                                        de los incentivos ofrecidos con base a la TABLA DE ASIGNACIÓN DE INCENTIVOS.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Una vez confirmado que el participante cumple con todos los requisitos, el
                                        sistema
                                        comunicará al “PARTICIPANTE” en la pantalla de www.elautobusmundialista.com.mx a
                                        cuál incentivo fue ganador acorde a la TABLA DE ASIGNACIÓN DE INCENTIVOS
                                        pudiendo
                                        ser uno de los siguientes: DESCUENTOS DEL 10% O 15%, BALONES O PLAYERAS y se le
                                        notificará vía correo electrónico, indicándole los requisitos para validar su
                                        participación según el incentivo obtenido.
                                    </p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <p>
                                DETERMINACIÓN DE POSIBLES GANADORES<sup>1</sup>: La determinación de posibles ganadores
                                se realizará
                                conforme al número de registro obtenido y de conformidad con la TABLA DE ASIGNACIONES.
                                Una vez confirmado que el posible ganador cumple con todos los requisitos, será
                                confirmado como ganador.
                            </p>
                            <p>
                            <ul>
                                <li>
                                    Cualquier información imprecisa o apócrifa que impida contactar al “PARTICIPANTE”,
                                    deja sin efectos su participación.</li>
                                <li>Es importante conservar el boleto registrado, ya que, en caso de ser necesario, el
                                    “ORGANIZADOR” lo solicitará al “PARTICIPANTE” para su validación. En ningún caso
                                    dicho boleto será retenido, únicamente será para efectos de validación del mismo.
                                </li>
                                <li>El “ORGANIZADOR” verificará la autenticidad de los datos proporcionados por el
                                    “PARTICIPANTE” por lo que en caso de resultar falsos dichos datos, se anulará el
                                    registro del “PARTICIPANTE” y no tendrá derecho a reclamar incentivo alguno. El
                                    “ORGANIZADOR” se reserva el derecho de verificar la autenticidad e inalterabilidad
                                    de los boletos registrados, así como de la información proporcionada por el
                                    “PARTICIPANTE” para la entrega de algún incentivo.</li>
                            </ul>
                            </p>
                        </li>
                        <li>
                            <p>CONTACTO CON POSIBLES GANADORES DE BALÓN Y PLAYERA:</p>
                            <p>
                                El participante una vez que haya comprado su boleto, efectuado su viaje y registrado en el sitio web www.elautobusmundialista.com.mx recibirá un correo electrónico con un código alfanumerico indicando el tipo de premio al que ha sido acreedor
                            <ul>
                                En dicho correo se le solicitará presentar en el centro de canje la siguiente documentación:
                                <li>Identificación oficial vigente con fotografía en original y copia por ambos lados</li>
                                <li>RFC con homoclave </li>
                                <li>Correo electrónico donde se le informa que ha sido ganador (debe ser legible el código alfanumérico y el premio asignado)</li>
                                <li>El boleto en original con el cual registró su participación en buen estado, es decir, sin tachaduras, ni enmendaduras y/o roto.
                                    Una vez finalizada la validación de documentos en el centro de canje se efectuará la entrega del premio detallada en el siguiente inciso.
                                    </li>
                            </ul>
                            </p>
                        </li>
                        <li>
                            <p>MECÁNICA PARA EL RECLAMO Y ENTREGA DE:
                            </p>
                            <ol type="a">

                                <li>
                                    <p>INCENTIVOS DE PLAYERAS Y BALONES:</p>
                                    <p>El “ORGANIZADOR” enviará al posible “GANADOR” del incentivo, un correo electrónico con un código alfanumérico y le indicará la ubicación de los 20 módulos dentro de las 20 terminales que fungirán como Centro de Canje, así como los horarios disponibles para hacer la entrega del incentivo ganado.</p>
                                    <p>
                                        Los BALONES Y PLAYERAS serán entregados solo en estos 20 módulos ubicados dentro de las 20 terminales que fungirán como Centro de Canje cada ganador deberá presentar original y copia de su INE vigente por ambos lados, proporcionar RFC con homoclave, correo electrónico donde se le informa que ha sido ganador (debe ser legible el código alfanumérico y el premio asignado) El boleto en original con el cual registró su participación en buen estado, es decir, sin tachaduras, ni enmendaduras y/o roto y la firma de recibo de conformidad del premio al que ha sido acreedor.  NO SE REALIZARÁN ENVÍOS A DOMICILIO EN NINGÚN CASO.
                                    </p>
                                </li>
                                <li>
                                    <p>DESCUENTOS DEL 10% y 15%:</p>
                                    <p>Al momento de obtener un código alfanumérico con descuento del 10% o 15% dentro del “SITIO” de “www.elautobusmundialista.com.mx”,  el ganador recibirá un mail al correo que registró con el descuento al que ha sido acreedor para que lo redima en taquillas en las marcas y rutas participantes. Solo debe presentar el codigo alfanumérico y una identificación oficial.</p>

                                </li>
                            </ol>
                        </li>
                        <li>
                            <p>
                               TÉRMINOS, CONDICIONES Y RESTRICCIONES GENERALES DE LA “PROMOCIÓN”:
                            </p>
                            <p>
                            <ol type="a">
                                <li>
                                    <p>Ningún incentivo ofrecido, podrá ser canjeado bajo ningún motivo por cantidad en efectivo</p>
                                </li>
                                <li>
                                    <p>En caso que por cualquier causa, el ganador(es) no pudiera(n) recoger, utilizar y/o hacer efectivo el incentivo obtenido por razones ajenas al “ORGANIZADOR” no tendrá derecho a exigir compensación alguna.</p>
                                </li>
                                <li>
                                    <p>Por la naturaleza del incentivo ofrecido, el “GANADOR” deberá de recoger su incentivo en la fecha y lugar en donde se le indique presentando su identificación oficial vigente así como el comprobante con su código alfanumérico impreso enviado por el “ORGANIZADOR” vía correo electrónico así como efectuar la suscripción de los documentos relativos a la recepción del incentivo.</p>
                                </li>
                                <li>
                                    <p>No podrán participar los empleados de ADO y Empresas Coordinadas S.A. de C.V., y sus empresas filiales o subsidiarias,  ni de los terceros que en su carácter de prestadores de servicios con cualquiera de las empresas antes mencionadas, tengan una intervención directa o indirecta en la planeación y ejecución de la “PROMOCIÓN”, ni sus filiales o subsidiarias, ni los cónyuges o familiares hasta el segundo grado de parentesco, así como toda persona relacionada o involucrada en la producción y desarrollo de la “PROMOCIÓN”.</p>
                                </li>
                                <li>
                                    <p>Los incentivos indicados no incluyen ninguna otra prestación y/o servicio adicional distinto a los estipulados o detallados por el “ORGANIZADOR” y podrán ser sustituidos en caso de inexistencia en su inventario.</p>
                                </li>
                                <li>
                                    <p>ADO y Empresas Coordinadas S.A. de C.V., no será responsable por ningún daño, perjuicio o pérdida ocasionada al “GANADOR” o cualquier otro tercero en sus personas y/o bienes, debido a/o con motivo o en relación con el uso y goce de los incentivos.
                                    </p>
                                </li>
                                <li>
                                    <p>El Organizador se reserva el derecho a descalificar a cualquier persona que ponga en riesgo la integridad y buen desarrollo de la promoción, como en el caso de “hackers” (término utilizado para referirse a personas con grandes conocimientos en informática y telecomunicaciones que son empleados con objetivos personales con fines de lucro, el cual puede o no ser maligno o ilegal) o “caza promociones” (definido como todo aquel participante que actúa solo o conjuntamente con recursos económicos, materiales o informativos de forma desleal frente a los otros participantes para obtener el beneficio de la promoción sin importar que los mecanismos que usa sean poco éticos, morales o incluso ilegales). Cualquier intento o ejecución de aplicación de programas o sistemas que pretendan atacar o vulnerar la promoción podrá ser perseguido con las acciones legales pertinentes que a criterio del Organizador sean suficientes y acarrea la inmediata descalificación y anulación de participación del consumidor participante que incurra en tal conducta, ante  cualquier sospecha referente a que el participante hubiere utilizado bots o cualquier otro tipo de herramientas de uso automatizado para el registro de los  códigos o para su reproducción apócrifa y actúe de un modo que infrinja alguna de las políticas de estas bases publicadas o en cualquier actividad o práctica que afecte negativamente la imagen de la marca organizadora de la promoción o que desacredite y/o menoscabe la reputación y buen nombre, será inmediatamente descalificado sin responsabilidad alguna y no tendrá derecho a participar nuevamente, sin que dicha descalificación limite el derecho del organizador de ejercer las acciones legales que considere procedentes en contra de él o los que resulten responsables. </p>
                                </li>
                                <li>
                                    <p>En caso de que el “GANADOR” de alguno de los incentivos requiera realizar algún gasto inherente para su entrega, uso o para llevarlo consigo a su lugar de origen, tales como transporte, alimentos, hospedaje, combustible, seguros, peajes, limpieza y/o mantenimiento, y/o cualquier otro, correrán por su propia cuenta y no le serán reembolsables de forma alguna.</p>
                                </li>
                                <li>
                                    <p>ADO y Empresas Coordinadas S.A. de C.V. no se hace responsable por las fallas ajenas que el sistema de red pudiera sufrir de manera temporal o definitiva, dentro de la vigencia de la “PROMOCIÓN” en la prestación del servicio del canal electrónico utilizado o en caso de que sean resultado de causas de fuerza mayor tales como desastres naturales, contingencias sanitarias o cualesquiera otra que no estén en control directo del “ORGANIZADOR”.</p>
                                </li>
                                <li>
                                    <p>No aplica el incentivo en caso de cancelación de la compra del boleto
                                        participante.
                                    </p>
                                </li>
                                <li>
                                    <p>El ganador está de acuerdo en que al participar en la presente “PROMOCIÓN”  autoriza a ADO y Empresas Coordinadas SA de CV, para que por sí o través de terceros utilicen gratuitamente su voz e imagen en cualquier medio y forma ya sea impreso, plástico, gráfico, visual, audiovisual, electrónico, fotográfico u otro similar en los Estados Unidos Mexicanos o en el extranjero con fines comerciales o informativos relacionados con la “PROMOCIÓN”.</p>
                                </li>
                                <li>
                                    <p>En virtud de lo anterior, el “GANADOR” y cada “PARTICIPANTE” en lo individual renuncian a cualquier retribución económica por el uso y explotación de su nombre e imagen, así como al ejercicio de cualquier acción civil, administrativa y penal en contra de ADO y Empresas Coordinadas SA de CV, o alguna otra empresa filial del grupo y relacionada con los aspectos señalados.</p>
                                </li>
                                <li>
                                    <p>ADO y Empresas Coordinadas S.A. de C.V., no será responsable por fallas en los equipos de computación, de comunicación, de suministro de energía, de líneas telefónicas, de la red de Internet, ni por desperfectos técnicos, errores humanos, virus informáticos o cualesquiera otros programas o esquemas maliciosos que pudieran ocasionar pérdidas de información, robo o suplantación de identidad o interrupción de negocios; o acciones de terceros que pudieran perturbar el normal desarrollo de la “PROMOCIÓN”.</p>
                                </li>
                                <li>
                                    <p>Derecho de eliminación de participaciones fraudulentas: ADO y Empresas Coordinadas S.A. de C.V. se reserva el derecho de eliminar justificadamente a cualquier usuario que defraude, altere o inutilice el buen funcionamiento y el transcurso normal y reglamentario de la presente “PROMOCIÓN”.</p>
                                </li>
                                <li>
                                    <p>El “ORGANIZADOR” pretende que todos los usuarios participen en igualdad de
                                        condiciones y con estricto respeto a las normas de la buena fe, por ello,
                                        cualquier
                                        utilización abusiva o fraudulenta de estas “BASES” dará lugar a la
                                        descalificación
                                        y/o anulación del usuario de cualquier promoción que realice dentro de ésta.</p>
                                </li>
                                <li>
                                    <p>Cuando por circunstancias no previstas, de caso fortuito o fuerza mayor que lo
                                        justifique el “ORGANIZADOR” podrá cancelar, suspender o modificar esta
                                        “PROMOCIÓN”
                                        previa notificación a la autoridad competente.</p>
                                </li>
                                <li>
                                    <p>Modificaciones y/o Anexos: El “ORGANIZADOR” se reserva el derecho de realizar
                                        modificaciones o añadir anexos sucesivos sobre su mecánica y beneficios, los
                                        mismos
                                        estén justificados o no perjudiquen a los participantes y se comuniquen a éstos
                                        debidamente.</p>
                                </li>
                                <li>
                                    <p>Ni ADO y Empresas Coordinadas S.A. de C.V., ni sus afiliadas, filiales,
                                        subsidiarias, accionistas, socios, empleados, factores y/o dependientes tendrán
                                        responsabilidad alguna en los registros de participación de los usuarios que
                                        sean
                                        erróneos, por causa de error ortográfico o tipográfico o por cualquier otra
                                        causa de
                                        fuerza mayor o caso inesperado o fortuito.</p>
                                </li>
                                <li>
                                    <p>No existe límite de boletos a registrar, solo en el caso de que el sistema
                                        detecte
                                        que se pretende ingresar el mismo boleto de viaje tres veces, se dará de baja al
                                        usuario y se procederá a la cancelación de su participación sin derecho a
                                        reclamar
                                        incentivo alguno y se le bloqueará del sitio.</p>
                                </li>
                                <li>
                                    <p>El contenido del “SITIO WEB” es propiedad de ADO y Empresas Coordinadas S.A. de
                                        C.V.
                                        por lo que cualquier uso indebido que se haga de los derechos de propiedad
                                        intelectual que lo integran, tales como la imagen, las marcas, avisos
                                        comerciales,
                                        nombres de dominio o información, será sujeto a los procedimientos y sanciones
                                        contenidas en la Ley de la materia. </p>
                                </li>
                                <li>
                                    <p>Aplica transferencia de boletos de la siguiente manera:  La transferencia de boletos solo podrá realizarse en taquilla, solo son transferibles para fecha y hora dentro del periodo de la promoción, sin poder transferir el boleto un tercero (otra persona).  La devolución de la posible diferencia a favor del cliente aplicará solamente cuando el pago se haya hecho en efectivo. Si la compra del boleto adquirido con este beneficio se realizó con Tarjeta de Crédito o Débito, solo será transferible pagando la diferencia correspondiente en Taquilla. La diferencia no es reembolsable con esta forma de pago, debido al precio preferencial otorgado. *Asientos sujetos a disponibilidad, rutas participantes consúltalos en: www.ado.com.mx o directamente en taquillas con nuestro personal. *Marcas participantes: ADO, ADO AEROPUERTO (Excepto vehículos pequeños tipo Sprinter) ADO GL, ADO PLATINO, OCC, ESTRELLA DE ORO, PLUSS, DIAMANTE Y AU (Servicio Directo Económico). Información, dudas o aclaraciones escribir a solucioneshola@ado.com.mx o mandar un mensaje de Whatsapp al: 5543864652.</p>
                                </li>
                                <li>
                                    <p>Al margen de lo que dispone la Ley Federal de Protección al Consumidor y de la NOM- 028-SCFI-2007 se hace pública la presente promoción denominada “SUBETE A LA EMOCIÓN DE QATAR”, por tratarse de una promoción por medio de un concurso en el cual el participante requiere de su habilidad y destreza para poder obtener un incentivo, la misma no se sujeta a la obtención de un permiso expedido por la Secretaría de Gobernación.</p>
                                </li>

                                <li>
                                    <p>
                                        <p> RESTRICCION ESPECIFICA DE INCENTIVOS CONSISTENTES EN DESCUENTOS DEL 10% Y 15%:</p>
                                        Podrá ser utilizado para comprar un boleto de viaje sencillo de autobús en el periodo de vigencia de la promoción, en las marcas y rutas participantes*. Vigencia de redención del descuento: Del 03 octubre al 15 de diciembre del 2022 para canje del código alfanumérico y para viajar (periodo de viaje) del 03 octubre al 15 de diciembre del 2022. El 10% o 15% de descuento aplica para canjear directamente en taquilla.  El descuento se aplicará al valor del viaje entero sencillo en la compra de los boletos en la modalidad de Compra Anticipada o Por Autobús Pagas Menos o tarifa entera normal. El descuento no aplica en la compra de los boletos de INAPAM, NIÑO, FUERZAS ARMADAS, PASAJERO QUEDADO, ESTUDIANTES, MAESTROS Y BOLETOS ABIERTOS. El descuento en la tarifa en los boletos de las modalidades de Compra Anticipada o Por Autobús Pagas Menos o tarifa entera normal NO es acumulable con otras promociones o descuentos. Los boletos adquiridos con este descuento NO SON CANCELABLES, pero si son trasferibles por fecha y hora dentro del periodo de la promoción, sin poder tranferir el boleto a otra persona. No aplica con las siguientes formas de pago: Orden de servicio, TURISSTE, cupón Kidde, cupón COTEMAR, pases de traslados, pases de vacaciones, cupón Interjet, MAYAPASS, cupón (USA) y cupón efectivo.
                                    </p>
                                </li>
                            </ol>
                            </p>
                        </li>
                        <li>
                            <p>CAUSAS QUE ANULAN LA PARTICIPACIÓN:</p>
                            <ul>
                                <li>
                                    <p>La falta de entrega de cualquiera de los documentos e información que solicite el organizador de la Promoción conforme a las presentes Bases. </p>
                                </li>
                                <li>
                                    <p>En caso de haber resultado acreedor a algún incentivo violentando las Bases de participación se deberá de restituir el mismo al organizador y responsable de la promoción, y responder contra daños y perjuicios. </p>
                                </li>
                                <li>
                                    <p>No serán válidas las participaciones registradas o hechas fuera de los plazos especificados de la Promoción, siendo descalificadas sin derecho a incentivos. </p>
                                </li>
                                <li>
                                    <p>Cualquier ataque o intento de alteración o manipulación al sistema informático, o a la página web o al lugar en el cual se encuentre radicada la misma, genera la anulación de la participación inmediata del Participante.  </p>
                                </li>
                                <li>
                                    <p>Los participantes que se dediquen a perpetuar en las promociones para fines ilícitos y desventajosos que delimiten la particpación de las demás interesadas </p>
                                </li>
                                <li>
                                    <p>Cualquier beneficiario de incentivo deberá proporcionar su R.F.C., por tener implícito el cumplimiento de obligaciones fiscales</p>
                                </li>
                                <li>
                                    <p>Quienes realicen cualquiera de estas conductas, responderán por daños o perjuicios de forma incondicional ante quienes reclamen acción de cualquier índole por tal situación. </p>
                                </li>
                                <li>
                                    <p>Cuando efectúen conductas o proporcionen información o imágenes que sean ilegales o contrarias a las buenas costumbres y ética. </p>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <p>PLAZO, LUGAR Y HORA PARA RECOGER EL INCENTIVO:</p>
                            <p>El “PARTICIPANTE” ganador a un incentivo podrá canjearlo de la siguiente forma:</p>
                            <ul>

                                <li>
                                    <p>Playera o Balón: El ganador recibirá vía correo electrónico un código
                                        alfanumérico y los requisitos para el seguimiento y verificación necesarios de
                                        su registro y poder canjear el incentivo obtenido en las terminales
                                        participantes (VER ANEXO “B").
                                    </p>
                                </li>
                                <li>
                                    <p>Descuento del 10%: El ganador recibirá vía correo electrónico un código
                                        alfanumérico y podrá canjear dicho descuento durante la vigencia establecida, en
                                        cualquier terminal o punto de venta, en las rutas de las marcas participantes.
                                    </p>
                                </li>
                                <li>
                                    <p>Descuento del 15%: El ganador recibirá vía correo electrónico un código
                                        alfanumérico y podrá canjear dicho descuento durante la vigencia establecida, en
                                        cualquier terminal o punto de venta, en las rutas de las marcas participantes.
                                    </p>
                                </li>
                                <li>
                                    <p>El “PARTICIPANTE” ganador a “PLAYERA o BALÓN” en la presente “PROMOCIÓN” tendrá hasta el 15 de diciembre de 2022 para reclamar su incentivo tomando en cuenta los centros de canje en las terminales participantes, durante los días y horarios mencionados en las presentes bases. Para mayor información, dudas o aclaraciones escribir a solucioneshola@ado.com.mx o Whatsapp: 5543864652.
                                    </p>
                                </li>
                                <li>
                                    <p>En caso de no hacerlo durante el plazo establecido, se entenderá que el
                                        “PARTICIPANTE” ganador renuncia al incentivo ganado, pudiendo disponer el
                                        “ORGANIZADOR” de este incentivo, como mejor convenga a sus intereses.
                                    </p>
                                </li>
                                <li>
                                    <p>La forma de asignar los incentivos ofrecidos, será mediante un registro
                                        simultáneo, ascendente, progresivo y continuo, por cada boleto de viaje
                                        registrado por el “PARTICIPANTE” y el cuál se inscribirá dentro de un sistema
                                        automático de registro. A cada boleto de viaje registrado correctamente,
                                        corresponde un incentivo siempre y cuando se cumpla de manera correcta y
                                        completa con los requisitos de participación.
                                    </p>
                                </li>
                                <li>
                                    <p>La veracidad de la compra del boleto y la aplicación de éste así como los datos
                                        proporcionados por el “PARTICIPANTE” son el punto de partida para la entrega del
                                        incentivo. Si alguno de los datos proporcionados en el registro de la
                                        “PROMOCIÓN” es falso, se anulará el registro del “PARTICIPANTE” y por ende su
                                        incentivo, y el participante no tendrá derecho a realizar reclamación alguna.
                                    </p>
                                </li>
                                <li>
                                    <p>Cada boleto sólo se puede registrar una sola vez. En caso de encontrar en los
                                        registros un boleto duplicado, el “PARTICIPANTE” será bloqueado y descalificado
                                        y pierde el derecho a reclamar cualquier incentivo de la “PROMOCIÓN”.
                                    </p>
                                </li>
                                <li>
                                    <p>En caso de que concluya la vigencia de la “PROMOCIÓN” sin que se hayan alcanzado
                                        los folios participantes y/o el registro del “PARTICIPANTE” de la “PROMOCIÓN”
                                        sea anulado, el “ORGANIZADOR” dispondrá de los incentivos como mejor convenga a
                                        sus intereses.
                                    </p>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <p>PUBLICACIÓN DE GANADORES:</p>
                            <p>La publicación de ganadores a incentivos (balones y playeras) se realizará el día Domingo de cada semana a partir del día 04 de octubre al 15 de diciembre del 2022, dentro del “SITIO WEB” www.elautobusmundialista.com.mx.</p>

                        </li>
                        <li>
                            <p>MEDIOS DE DIFUSIÓN:</p>
                            <p>Las bases de la “PROMOCIÓN” se difundirán a través de los siguientes medios:</p>
                            <ol>

                                <li>
                                    <p> En el “SITIO” www.elautobusmundialista.com.mx
                                    </p>
                                </li>
                                <li>
                                    <p> En Terminales y Puntos de venta (Pizarrones informativos) con términos y
                                        condiciones
                                        generales de la “PROMOCIÓN”.</p>
                                </li>

                                <li>
                                    <p> En Terminales y Puntos de venta solicitándolo al personal de atención en
                                        taquillas.
                                    </p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <p>AUTORIZACIÓN DE USO DE IMAGEN:</p>
                            <p> El ganador, al recibir su incentivo, autoriza de manera gratuita, a ADO y Empresas
                                Coordinadas S.A. de C.V., la toma de fotografías y grabaciones y a la comunicación
                                pública de las mismas, en los medios de comunicación que el “ORGANIZADOR” de la
                                “PROMOCIÓN” estime conveniente, dentro de los Estados Unidos Mexicanos o el extranjero,
                                con fines publicitarios respecto de su participación en la “PROMOCIÓN” e informativos
                                relacionados con la “PROMOCIÓN”.
                            </p>
                            <p>En virtud de lo anterior, el “GANADOR” renuncia a cualquier retribución económica por el
                                uso y explotación de su nombre e imagen, así como al ejercicio de cualquier acción
                                civil, administrativa y/o penal en contra de ADO y Empresas Coordinadas S.A. de C.V.,
                                sus empresas filiales, subsidiarias o relacionadas por los aspectos antes señalados.
                            </p>
                            <p> El “PARTICIPANTE” se compromete a firmar cualquier documento que el “ORGANIZADOR” estime
                                pertinente para autorizarle al “ORGANIZADOR” la toma y uso de fotografías y grabaciones
                                a través de cualquier medio de comunicación conocido o por conocerse.
                            </p>
                            <p> Tanto las fotografías como las grabaciones de participación a que se hacen referencia en
                                el párrafo anterior, serán propiedad exclusiva del “ORGANIZADOR” de la PROMOCIÓN, por lo
                                que el “ORGANIZADOR” podrá autorizar a los titulares de las marcas participantes en la
                                “PROMOCIÓN”, el uso y comunicación pública de las fotografías y grabaciones antes
                                mencionadas, sin que esto implique pago alguno a dichos “GANADORES” por su aparición en
                                cualquier medio de comunicación.
                            </p>
                        </li>
                        <li>
                            <p>AVISO DE PRIVACIDAD:</p>
                            <p>El “ORGANIZADOR” se compromete firmemente a proteger la privacidad de su información
                                personal.
                            </p>
                            <p>El “PARTICIPANTE” consiente el recabo y tratamiento de sus datos personales para ejecutar
                                la “PROMOCIÓN” y entregar el incentivo. Para mayor información acerca del tratamiento y
                                de los derechos que puede hacer valer, usted puede consultar nuestro aviso de privacidad
                                integral en la dirección electrónica https://www.ado.com.mx/aviso-de-privacidad, así
                                como consultarlo físicamente en los módulos de canje correspondientes.
                            </p>
                        </li>
                        <li>
                            <p>
                                INFORMACIÓN EN INTERNET / USO DE COOKIES
                            </p>
                            <p>Nuestra página cuenta con tecnologías electrónicas (cookies y web beacons) por lo que cuando usted accede, es recibida por nuestra parte, información referente a cómo es su tipo de navegador y sistema operativo, las páginas de internet que ha visitado recientemente, los vínculos que recientemente ha seguido, la dirección de IP de su computadora y el sitio que cerró antes de entrar a nuestra página de internet.  Utilizamos esta información para medir la cantidad de personas que visitan nuestra página de internet y sus diversas secciones, y así mejorar el funcionamiento de la misma: https://www.elautobusmundialista.com.mx/. Usted puede deshabilitar o ajustar el uso de estas tecnologías siguiendo los procedimientos del navegador de internet que utiliza para acceder a nuestra página de internet. Asi mismo, hacemos de su conocimiento que al navegar o usar nuestra página de internet: https://www.elautobusmundialista.com.mx/ usted podría encontrar hipervínculos, links, banners, botones, propiedad de terceros que podrían solicitar sus datos personales (en adelane “Sitios de Terceros”). Los datos personales que usted proporcione a través de estos portales o sitios de internet se sujetarán a los Avisos de Privacidad desplegados y aplicables a estos portales y sitios de Internet, por tanto no seremos responsables por el uso, publicación, revelación y/o divulgación que usted haga respecto de sus datos personales a través de los Sitios de Terceros antes enunciados. Le recomendamos ser cuidadoso y responsable sobre la información de carácter personal que proporcione en dichos sitios.</p>
                            <p>Se entiende para efectos del presente aviso lo siguiente: </p>
                            <ol>

                                <li>
                                    <p>COOKIES: son los archivos de texto que se descargan automáticamente y se almacenan en el disco duro del equipo de cómputo del usuario al navegar en una página o portal de internet especifico, que permiten almacenar al servidor de internet ciertos datos, entre ellos, información sobre sus preferencias y pautas de navegación
                                    </p>
                                </li>
                                <li>
                                    <p> Web beacons: Es una imagen usada exclusivamente para cuantificar el número de visitas o monitorear el comportamiento del usuario.</p>
                                </li>
                            </ol>
                        </li>
                        <li>
                            <p>JURISDICCIÓN APLICABLE: </p>
                            <p> Cualquier controversia suscitada por la Promoción y sus complementarias, se regirá por las presentes Bases y como complemento para lo establecido en éstas aplicará la jurisdicción de la Ciudad de México, sujetándose todas las partes interesadas a las mismas renunciando al que por beneficio de su domicilio presente o futuro pudiese corresponderles. </p>

                        </li>
                    </ol>
                    <p><strong>“ANEXO A” TABLA DE ASIGNACIÓN DE INCENTIVOS. Se adjunta al presente mediante FORMATO USB en virtud del número de participaciones.</strong> </p>
                    <p><strong>“ANEXO B”. CENTROS DE CANJE</strong> </p>
                    <p>Los centros de canje estarán operando en las siguientes ciudades durante la misma vigencia de la promoción. Del 04 de octubre a 15 de diciembre 2022 en los horarios establecidos para centro, conforme a lo siguiente:</p>
                    <div class="table-responsive">
                    <table class="table table-bordered border-primary">
                            <thead class="table-primary text-center">
                                <tr>
                                    <th>No.</th>
                                    <th>TERMINAL Y/O PUNTO DE VENTA</th>
                                    <th>DIRECCIÓN</th>
                                    <th>C.P.</th>
                                    <th>HORARIOS DE ATENCIÓN PARA REGISTRO Y/O CANJE EN PRODUCTOS
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-justify">Veracruz </td>
                                    <td class="text-justify">AV. Salvador Díaz Mirón No. 1698 - Col. Centro, Veracruz.
                                    </td>
                                    <td class="text-justify">91700</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">2</td>
                                    <td class="text-justify">Córdoba</td>
                                    <td class="text-justify">Prol. Ave. 4 s/n entre calles 39 y 41, Col. Puente Bejuco,
                                        Córdoba,
                                        Veracruz.</td>
                                    <td class="text-justify">94690</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">3</td>
                                    <td class="text-justify">Xalapa</td>
                                    <td class="text-justify">Av. 20 de noviembre Oriente No. 571, Fraccionamiento Unidad
                                        Pomona,
                                        Xalapa, Veracruz.</td>
                                    <td class="text-justify">91040</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">4</td>
                                    <td class="text-justify">CAPU</td>
                                    <td class="text-justify"> Boulevard Norte 4222. Col. Las Cuartillas, Puebla.</td>
                                    <td class="text-justify">72050</td>
                                    <td class="text-justify">De lunes a sábado de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">5</td>
                                    <td class="text-justify">Teziutlán</td>
                                    <td class="text-justify">Calle Zaragoza , sin número, esq. Allende. Teziutlán,
                                        Puebla.
                                    </td>
                                    <td class="text-justify">73800</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">6</td>
                                    <td class="text-justify">Mérida Centro Histórico</td>
                                    <td class="text-justify">Por 68 y 70 C.69 - 554 Centro, Mérida, Yucatán</td>
                                    <td class="text-justify">97000</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">7</td>
                                    <td class="text-justify">Ciudad del Carmen</td>
                                    <td class="text-justify">Av. Luis Donaldo Colosio entre 20 de noviembre y Francisco
                                        Villa,
                                        Col. Francisco I. Madero, Cd. del Carmen, Campeche. </td>
                                    <td class="text-justify">24190</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">8</td>
                                    <td class="text-justify">Cancún</td>
                                    <td class="text-justify">Calle Pino, entre Ave. Tulum y Ave. Uxmal, Cancún, Quintana
                                        Roo.
                                    </td>
                                    <td class="text-justify">77500</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">9</td>
                                    <td class="text-justify">Chetumal Led</td>
                                    <td class="text-justify">Av. Palermo No 398, 179 Col. 20 de Noviembre,Chetumal,
                                        Quintana
                                        Roo.</td>
                                    <td class="text-justify">77038</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">10</td>
                                    <td class="text-justify">Tuxtla (OCC)</td>
                                    <td class="text-justify">Boulevard Antonio Pariente Algarín No 551 esquina Av. 5ta
                                        Norte
                                        Poniente Colonia Los Cafetales, Tuxtla Gutiérrez, Chiapas.</td>
                                    <td class="text-justify">29030</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">11</td>
                                    <td class="text-justify">Tapachula (OCC) </td>
                                    <td class="text-justify">Calle 17 Ote. No. 45, Col.Centro, Tapachula, Chiapas.</td>
                                    <td class="text-justify">30700</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">12</td>
                                    <td class="text-justify">Oaxaca</td>
                                    <td class="text-justify">Calle 5 de Mayo No. 900, Col. Centro, Oaxaca, Oaxaca de
                                        Juárez
                                    </td>
                                    <td class="text-justify">68000</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">13</td>
                                    <td class="text-justify">Chilpancingo</td>
                                    <td class="text-justify">C. 21 de Marzo, Benito Juárez, Chilpancingo de los Bravo,
                                        Guerrero
                                    </td>
                                    <td class="text-justify">39010</td>
                                    <td class="text-justify">De lunes a sábado de 10 a.m.  a 6 p.m. </td>
                                </tr>
                                <tr>
                                    <td class="text-center">14</td>
                                    <td class="text-justify">Acapulco</td>
                                    <td class="text-justify">Av Cuauhtémoc 1490, Fracc Magallanes, Marroquin, Acapulco
                                        de
                                        Juárez, Gro.</td>
                                    <td class="text-justify">39670</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">15</td>
                                    <td class="text-justify">Villahermosa</td>
                                    <td class="text-justify">Av. Fco Javier Mina esq. Lino Merino S/N Col. Centro,
                                        Villahermosa,
                                        Tabasco.</td>
                                    <td class="text-justify">86000</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">16</td>
                                    <td class="text-justify">Coatzacoalcos</td>
                                    <td class="text-justify">Avenida Transismica 100-A Col. Nueva Obrera. Villas del
                                        Sur,
                                        Coatzacoalcos, Veracruz </td>
                                    <td class="text-justify">96598</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">17</td>
                                    <td class="text-justify">TAPO</td>
                                    <td class="text-justify">Calzada Ignacio Zaragoza No.200, Col. Siete de Julio,
                                        Alcaldía
                                        Venustiano Carranza. CDMX</td>
                                    <td class="text-justify">15390</td>
                                    <td class="text-justify">De lunes a sábado de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">18</td>
                                    <td class="text-justify">Poza Rica</td>
                                    <td class="text-justify">Av. Puebla, s/n, Col. Palma Sola, Poza Rica, Veracruz </td>
                                    <td class="text-justify">91477</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">19</td>
                                    <td class="text-justify">Tampico</td>
                                    <td class="text-justify">Av. Rosalio Bustamante No. 210 Col. Allende, Tampico,
                                        Tamaulipas.
                                    </td>
                                    <td class="text-justify">89130</td>
                                    <td class="text-justify">Viernes y sábados de 10 a.m.  a 6 p.m.</td>
                                </tr>
                                <tr>
                                    <td class="text-center">20</td>
                                    <td class="text-justify">Terminal Norte</td>
                                    <td class="text-justify">Eje Central Lázaro Cárdenas 271, Magdalena de las Salinas,
                                        Gustavo
                                        A. Madero, CDMX.</td>
                                    <td class="text-justify">7760</td>
                                    <td class="text-justify">De lunes a sábado de 10 a.m.  a 6 p.m.</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <p>
                        <sup>1</sup> Posible ganador: Es aquel que acredite cumplir con las presentes bases legales y
                        haga entrega de la documentación solicitada de forma completa y correcta para hacer la
                        reclamación del incentivo. Una vez confirmado lo anterior, se procederá a la confirmación en
                        calidad de “Ganador”.
                    </p>
                </div>
            </div>

            <div class="text-center mt-4 pt-4">
                <a target="_blank" href="https://drive.google.com/u/0/uc?id=162Yog3Azr3utf-wUAu6Yl7Ll1Esf6POA&export=download" class="btn btn-lg btn-success">Consulta la tabla de incentivos</a>
            </div>
            <div class="text-center mt-4 pt-4">
                <a target="_blank" href="https://drive.google.com/u/0/uc?id=1tIYW_njqrhN9vacxa48QWG2kfFEzntNQ&export=download" class="btn btn-lg btn-success">Conoce a nuestros ganadores</a>
            </div>
        </div>

    </div>
    <div class="aficionados"></div>
</div>
@endsection
