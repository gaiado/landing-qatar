<table class="table table-bordered border-primary">
    <thead class="table-primary text-center">
        <tr>
            <th>No.</th>
            <th>TERMINAL Y/O PUNTO DE VENTA</th>
            <th>DIRECCIÓN</th>
            <th>C.P.</th>
            <th>HORARIOS DE ATENCIÓN PARA REGISTRO Y/O CANJE EN PRODUCTOS
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">1</td>
            <td class="text-justify">Veracruz </td>
            <td class="text-justify">AV. Salvador Díaz Mirón No. 1698 - Col. Centro, Veracruz.
            </td>
            <td class="text-justify">91700</td>
            <td class="text-justify">7-8 de octubre / 11-12 -18-19-22-23- 24-25-26-27 de noviembre / 9-10 diciembre 
de 10 a.m. a 6 p.m.
            </td>
        </tr>
        <tr>
            <td class="text-center">2</td>
            <td class="text-justify">Córdoba</td>
            <td class="text-justify">Prol. Ave. 4 s/n entre calles 39 y 41, Col. Puente Bejuco,
                Córdoba,
                Veracruz.</td>
            <td class="text-justify">94690</td>
            <td class="text-justify">7-8 de octubre / 11-12-22-23-24-25-26-27 de noviembre / 9-10 diciembre 
de 10 a.m. a 6 p.m.
                             
            </td>
        </tr>
        <tr>
            <td class="text-center">3</td>
            <td class="text-justify">Xalapa</td>
            <td class="text-justify">Av. 20 de noviembre Oriente No. 571, Fraccionamiento Unidad
                Pomona,
                Xalapa, Veracruz.</td>
            <td class="text-justify">91040</td>
            <td class="text-justify">7- 8 de octubre / 11-12- 22 -23 - 24 - 25 -26-27 de noviembre / 9 - 10 diciembre 
de 10 a.m. a 6 p.m.
                
            </td>
        </tr>
        <tr>
            <td class="text-center">4</td>
            <td class="text-justify">CAPU</td>
            <td class="text-justify"> Boulevard Norte 4222. Col. Las Cuartillas, Puebla.</td>
            <td class="text-justify">72050</td>
            <td class="text-justify">4-5-6-7-8 de octubre / 7-8-9-10-11-12-14, 15,16 -17-18-19,  22-23 -24-25-26-27 de noviembre / 2-3-5-6 -7- 8- 9-10 de diciembre 
de 10 a.m. a 6 p.m. 
                
                
            </td>
        </tr>
        <tr>
            <td class="text-center">5</td>
            <td class="text-justify">Teziutlán</td>
            <td class="text-justify">Calle Zaragoza , sin número, esq. Allende. Teziutlán,
                Puebla.
            </td>
            <td class="text-justify">73800</td>
            <td class="text-justify">7-8 de octubre / 11-12 noviembre / 9 - 10 de diciembre 
de 10 a.m. a 6 p.m.  
                
            </td>
        </tr>
        <tr>
            <td class="text-center">6</td>
            <td class="text-justify">Mérida Centro Histórico</td>
            <td class="text-justify">Por 68 y 70 C.69 - 554 Centro, Mérida, Yucatán</td>
            <td class="text-justify">97000</td>
            <td class="text-justify">7-8 de octubre / 11-12- 22-23- 24 - 25-26-27 de noviembre / 9-10 diciembre 
de 10 a.m. a 6 p.m.
                
                
            </td>
        </tr>
        <tr>
            <td class="text-center">7</td>
            <td class="text-justify">Ciudad del Carmen</td>
            <td class="text-justify">Av. Luis Donaldo Colosio entre 20 de noviembre y Francisco
                Villa,
                Col. Francisco I. Madero, Cd. del Carmen, Campeche. </td>
            <td class="text-justify">24190</td>
            <td class="text-justify">7-8 de octubre / 11-12-22-23- 24 - 25-26-27 de noviembre / 9-10 diciembre 
de 10 a.m. a 6 p.m.
                
                
            </td>
        </tr>
        <tr>
            <td class="text-center">8</td>
            <td class="text-justify">Cancún</td>
            <td class="text-justify">Calle Pino, entre Ave. Tulum y Ave. Uxmal, Cancún, Quintana
                Roo.
            </td>
            <td class="text-justify">77500</td>
            <td class="text-justify">7-8 de octubre / 11-12- 22-23- 24 - 25 -26-27 de noviembre / 9-10 diciembre 
de 10 a.m. a 6 p.m.
                
            </td>
        </tr>
        <tr>
            <td class="text-center">9</td>
            <td class="text-justify">Chetumal Led</td>
            <td class="text-justify">Av. Palermo No 398, 179 Col. 20 de Noviembre,Chetumal,
                Quintana
                Roo.</td>
            <td class="text-justify">77038</td>
            <td class="text-justify"> 7-8 de octubre / 11-12 noviembre / 9-10 de diciembre 
de 10 a.m. a 6 p.m.  
                
                
            </td>
        </tr>
        <tr>
            <td class="text-center">10</td>
            <td class="text-justify">Tuxtla (OCC)</td>
            <td class="text-justify">Boulevard Antonio Pariente Algarín No 551 esquina Av. 5ta
                Norte
                Poniente Colonia Los Cafetales, Tuxtla Gutiérrez, Chiapas.</td>
            <td class="text-justify">29030</td>
            <td class="text-justify">7-8 de octubre / 11 – 12 noviembre / 9-10 de diciembre 
de 10 a.m. a 6 p.m.  

                
            </td>
        </tr>
        <tr>
            <td class="text-center">11</td>
            <td class="text-justify">Tapachula (OCC) </td>
            <td class="text-justify">Calle 17 Ote. No. 45, Col.Centro, Tapachula, Chiapas.</td>
            <td class="text-justify">30700</td>
            <td class="text-justify">7- 8 de octubre /11-12 noviembre / 9-10 de diciembre 
de 10 a.m. a 6 p.m.  
                
                
            </td>
        </tr>
        <tr>
            <td class="text-center">12</td>
            <td class="text-justify">Oaxaca</td>
            <td class="text-justify">Calle 5 de Mayo No. 900, Col. Centro, Oaxaca, Oaxaca de
                Juárez
            </td>
            <td class="text-justify">68000</td>
            <td class="text-justify">7-8 de octubre /11-12- 22-23-24 - 25-26-27 de noviembre / 9-10 diciembre
 de 10 a.m. a 6 p.m.
                
                
            </td>
        </tr>
        <tr>
            <td class="text-center">13</td>
            <td class="text-justify">Chilpancingo</td>
            <td class="text-justify">C. 21 de Marzo, Benito Juárez, Chilpancingo de los Bravo,
                Guerrero
            </td>
            <td class="text-justify">39010</td>
            <td class="text-justify">4-5- 6-7 8 de octubre / 7-8-9-10-11-12-14- 15-16-17-18-19- 22-23-24-25-26-27 de noviembre / 2-3- 5-6 -7- 8- 9-10 de diciembre 
de 10 a.m. a 6 p.m
                
                
            </td>
        </tr>
        <tr>
            <td class="text-center">14</td>
            <td class="text-justify">Acapulco</td>
            <td class="text-justify">Av Cuauhtémoc 1490, Fracc Magallanes, Marroquin, Acapulco
                de
                Juárez, Gro.</td>
            <td class="text-justify">39670</td>
            <td class="text-justify">7–8 de octubre / 11-12- 22-23-24-25-26-27 de noviembre / 9-10 diciembre 
de 10 a.m. a 6 p.m.
                
            </td>
        </tr>
        <tr>
            <td class="text-center">15</td>
            <td class="text-justify">Villahermosa</td>
            <td class="text-justify">Av. Fco Javier Mina esq. Lino Merino S/N Col. Centro,
                Villahermosa,
                Tabasco.</td>
            <td class="text-justify">86000</td>
            <td class="text-justify">7-8 de octubre / 11 -12-18-19-22-23- 24-25-26-27 de noviembre / 9–10 diciembre 
de 10 a.m. a 6 p.m.
                
            </td>
        </tr>
        <tr>
            <td class="text-center">16</td>
            <td class="text-justify">Coatzacoalcos</td>
            <td class="text-justify">Avenida Transismica 100-A Col. Nueva Obrera. Villas del
                Sur,
                Coatzacoalcos, Veracruz </td>
            <td class="text-justify">96598</td>
            <td class="text-justify">7-8 de octubre / 11 -12-18 -19-22-23-24-25 -26-27 de noviembre /  9-10 diciembre
 de 10 a.m. a 6 p.m.
               
            </td>
        </tr>
        <tr>
            <td class="text-center">17</td>
            <td class="text-justify">TAPO</td>
            <td class="text-justify">Calzada Ignacio Zaragoza No.200, Col. Siete de Julio,
                Alcaldía
                Venustiano Carranza. CDMX</td>
            <td class="text-justify">15390</td>
            <td class="text-justify">
               4 -5-6-7-8 octubre / 7-8-9-10-11-12 -14-18-19-22-23-24-25-26-27 de noviembre / 2-3 -5-6-7-8-9-10 de diciembre 
de 10 a.m. a 6 p.m.
                
            </td>
        </tr>
        <tr>
            <td class="text-center">18</td>
            <td class="text-justify">Poza Rica</td>
            <td class="text-justify">Av. Puebla, s/n, Col. Palma Sola, Poza Rica, Veracruz </td>
            <td class="text-justify">91477</td>
            <td class="text-justify">7-8 octubre / 11-12- 22-23-24-25-26-27 de noviembre / 9-10 diciembre 
de 10 a.m. a 6 p.m.

                
            </td>
        </tr>
        <tr>
            <td class="text-center">19</td>
            <td class="text-justify">Tampico</td>
            <td class="text-justify">Av. Rosalio Bustamante No. 210 Col. Allende, Tampico,
                Tamaulipas.
            </td>
            <td class="text-justify">89130</td>
            <td class="text-justify">7-8 octubre / 11-12 – 22-23-24-25-26-27 de noviembre / 9-10 diciembre 
de 10 a.m. a 6 p.m.
                
            </td>
        </tr>
        <tr>
            <td class="text-center">20</td>
            <td class="text-justify">Terminal Norte</td>
            <td class="text-justify">Eje Central Lázaro Cárdenas 271, Magdalena de las Salinas,
                Gustavo
                A. Madero, CDMX.</td>
            <td class="text-justify">7760</td>
            <td class="text-justify">
                4-5-6-7-8 de octubre, / 7-8-9-10-11-12 -13,  22-23- 24-25 -26-27 de noviembre / 2 -3-5 -6-7-8-9-10 de diciembre 
de 10 a.m. a 6 p.m.
            </td>
        </tr>
    </tbody>
</table>
