@extends('layouts.app')
@section('title')
Preguntas frecuentes
@endsection
<script id="ze-snippet"
        src="https://static.zdassets.com/ekr/snippet.js?key=3f0466b2-e6f2-49ae-8a17-5f2f89922cfc"> </script>
@section('content')
<div class="preguntas">
    <div class="wrapper pt-4">
        <div class="container pb-4 mb-4">
            <h2 class="titulo-balon">¿Tienes dudas sobre la promoción
                "Súbete a la emoción de Qatar”? Aquí las resolvemos</h2>

            <div class="accordion lista-preguntas" id="accordionExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            ¿CÓMO FUNCIONA LA MECÁNICA DE LA PROMOCIÓN “SÚBETE A LA EMOCIÓN DE QATAR”?
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p> Para participar debes adquirir un boleto para viajar en cualquier marca participante;
                                ADO, ADO Aeropuerto*, ADO GL, ADO Platino, OCC, ESTRELLA DE ORO Y AU*. Una vez que
                                abordaste la unidad, o concluido tu viaje, tu viaje podrás registrar tu boleto dentro
                                del sitio web de la promoción en www.elautobusmundialista.com.mx. Si requieres apoyo
                                para realizar tu registro puedes acudir en los días y horarios disponibles en cualquiera
                                de los 20 módulos de canje que se ubicarán en las distintas terminales y que puedes
                                consultar en la misma página de la “PROMOCIÓN” en la pestaña centros de canje. </p> *No
                            aplica en vehículos pequeños tipo Sprinter <br> *No aplica en Servicio Directo Económico
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon0">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse0" aria-expanded="false" aria-controls="collapse0">
                            ¿CUÁLES SON LOS PREMIOS POR LOS QUE PUEDO PARTICIPAR?
                        </button>
                    </h2>
                    <div id="collapse0" class="accordion-collapse collapse" aria-labelledby="heading0"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>Tendremos premios como playeras y balones oficiales de la Selección Nacional de México y
                                descuentos del 10 y 15% en tu próximo viaje. En total serán más de 5 millones de premios
                                a ofrecerse durante la vigencia de la promoción.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon1">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            ¿QUÉ DATOS DEBO REGISTRAR EN EL SITIO DE LA PROMOCIÓN?
                        </button>
                    </h2>
                    <div id="collapse1" class="accordion-collapse collapse" aria-labelledby="heading1"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p> Para participar debes registrarte en el sitio de la “PROMOCIÓN”
                                www.elautobusmundialista.com.mx con los siguientes datos. </p>
                            <p></p>
                            <ul>
                                <li>Correo electrónico (Es importante verifiques que esté correctamente escrito tu
                                    correo para que puedas recibir la confirmación de tu premio) y no olvidar tu
                                    contraseña, ya que con ella podrás reingresar a tu perfil para registrar más boletos
                                    y seguir ganando). </li>
                                <li>Nombre completo, escrito correctamente, ya que para poder canjear tu premio deberá
                                    coincidir con tu identificación oficial vigente.</li>
                                <li>Código postal</li>
                                <li>Número de folio y corrida/viaje del boleto con el cual viajaste.</li>
                            </ul>
                            <p></p>
                            <p>Una vez hecho correctamente el registro de tu boleto el sistema te informará qué premio
                                te corresponde y recibirás un correo electrónico que te indicará los pasos a seguir.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon2">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            ¿EL NOMBRE DEL BOLETO DEBE COINCIDIR CON EL NOMBRE DEL REGISTRO?
                        </button>
                    </h2>
                    <div id="collapse2" class="accordion-collapse collapse" aria-labelledby="heading2"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>Sí. Para poder participar en la promoción “Súbete a la emoción de Qatar” es necesario que
                                el boleto adquirido esté dentro de la vigencia establecida, que tenga tu nombre completo
                                escrito correctamente, ya que para poder canjear tu premio deberá coincidir con tu
                                identificación oficial vigente.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon3">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            ¿NO ENCUENTRO DÓNDE REGISTRARME EN EL SITIO DE LA PROMOCIÓN?
                        </button>
                    </h2>
                    <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="heading3"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <ul>
                                <li>Debes ingresar a la página de inicio de www.elautobusmundialista.com.mx y visualizar
                                    el texto que dice: “Súbete a la emoción de Qatar”. deberás dar click para iniciar tu
                                    sesión “¿No tienes Cuenta? REGÍSTRATE Aquí” y crea una cuenta nueva para poder
                                    participar.</li>
                                <li>Recuerda, al momento de tu registro debes llenar los datos solicitados
                                    correctamente: </li>
                                <li>Tu nombre completo (igual como aparece en tu identificación oficial vigente).</li>
                                <li>Correo electrónico (es importante verifiques que esté correctamente escrito tu
                                    correo para que puedas recibir la confirmación de tu premio y no olvidar tu
                                    contraseña, ya que con ella podrás reingresar a tu perfil para registrar más boletos
                                    y seguir ganando).</li>
                                <li>El código postal, de la ciudad en la que radicas o harás el canje de tu boleto.</li>
                                <li>Y tener a la mano tu boleto de viaje con el cual viajaste, este es necesario para
                                    registrar tu número de folio y corrida/viaje.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon4">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            ME REGISTRÉ Y GANÉ, PERO NO ME LLEGA MI CORREO DE CONFIRMACIÓN, ¿QUÉ HAGO?
                        </button>
                    </h2>
                    <div id="collapse4" class="accordion-collapse collapse" aria-labelledby="heading4"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p> Respecto a la recuperación de tu correo, es muy importante mandes un correo a:
                                solucioneshola@ado.com.mx con los siguientes datos: </p>
                            <ul>
                                <li>Tu nombre completo (igual como aparece en tu identificación oficial VIGENTE) y
                                    coincida con tu boleto de viaje.</li>
                                <li>Correo electrónico (es importante verifiques que esté correctamente escrito tu
                                    correo para que puedas recibir la confirmación de tu premio y no olvidar tu
                                    contraseña, ya que con ella podrás reingresar a tu perfil para registrar más boletos
                                    y seguir ganando)</li>
                                <li>Conocer tu código postal.</li>
                                <li>Número de folio y corrida/viaje del boleto con el cual viajaste.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon5">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                            ¿DEBO DE REGISTRAR MI BOLETO AL MOMENTO DE LA COMPRA?
                        </button>
                    </h2>
                    <div id="collapse5" class="accordion-collapse collapse" aria-labelledby="heading5"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>No. El boleto deberá ser adquirido y una vez que abordes la unidad o concluyas tu viaje
                                tendrás la posibilidad de registrar tu boleto a través del sitio
                                www.elautobusmundialista.com.mx, o en los módulos de canje dentro de la vigencia de la
                                promoción la cual termina el 15 de diciembre de 2022.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon6">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                            ¿PUEDO PARTICIPAR MÁS DE UNA VEZ?
                        </button>
                    </h2>
                    <div id="collapse6" class="accordion-collapse collapse" aria-labelledby="heading6"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>Sí claro, mientras adquieras un boleto para viajar en los autobuses de cualquier marca
                                participante ADO, ADO Aeropuerto*, ADO GL y ADO Platino y una vez realizado tu viaje
                                registres cada boleto correctamente, podrás seguir participando y ganando uno de los
                                millones de premios disponibles durante la vigencia de la “PROMOCIÓN”. Es importante
                                consultar las bases, términos y condiciones de la misma en la pestaña de BASES ubicada
                                en la página de la “PROMOCIÓN”, www.elautobusmundialista.com.mx o solicitando el
                                documento completo con tu asesor en taquilla.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon7">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                            ¿CUÁL ES MI NÚMERO DE FOLIO Y NÚMERO DE CORRIDA/VIAJE?
                        </button>
                    </h2>
                    <div id="collapse7" class="accordion-collapse collapse" aria-labelledby="heading7"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p> Si tu boleto es digital, es decir, lo compraste en www.ado.com.mx el número de folio
                                está en la parte superior del mismo y lo ubicarás como “No. Folio” y en la parte
                                inferior podrás localizar el número de corrida. </p>
                            <p> Si tu boleto lo compraste en taquilla, tu número de folio se encuentra justo en la parte
                                de abajo, arriba del código de barras y el número de corrida/viaje se encuentra como
                                “Corrida” en la parte media de tu boleto. También justo antes de realizar tu registro en
                                www.elautobusmundialista.com.mx puedes ubicar el botón de ayuda para que visualices su
                                ubicación. </p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon8">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                            ¿QUÉ VIGENCIA TIENE LA PROMOCIÓN?
                        </button>
                    </h2>
                    <div id="collapse8" class="accordion-collapse collapse" aria-labelledby="heading8"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>La promoción es válida al adquirir tus boletos a partir de las 00:01 del 04 de octubre al
                                15 de diciembre 2022. Durante ese periodo, la gente que adquiera un boleto para viajar
                                en los autobuses de las marcas ADO, ADO Aeropuerto*, ADO GL, ADO Platino, OCC, ESTRELLA
                                DE ORO Y AU tendrá la oportunidad de ganar uno de los premios que tiene. Es importante
                                consultar las bases, términos y condiciones de la misma en la pestaña de BASES ubicada
                                en la página de la “PROMOCIÓN” www.elautobusmundialista.com.mx o solicitando el
                                documento completo con tu asesor en taquilla.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon9">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                            ¿CÓMO SÉ QUE GANÉ UN PREMIO?
                        </button>
                    </h2>
                    <div id="collapse9" class="accordion-collapse collapse" aria-labelledby="heading9"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>Todos los ganadores recibirán un correo electrónico con el premio que obtuvieron así como
                                las instrucciones para el canje según el premio que hayan obtenido: playera, balón,
                                descuentos y las instrucciones para verificar los datos del participante y poder hacerse
                                acreedor a éste.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon10">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                            ¿DÓNDE Y CÓMO PUEDO CANJEAR MI PREMIO?
                        </button>
                    </h2>
                    <div id="collapse10" class="accordion-collapse collapse" aria-labelledby="heading10"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <ul>
                                <li>Descuento del 10%: El ganador recibirá vía correo electrónico un código alfanumérico
                                    y podrá canjear dicho descuento durante la vigencia establecida, en la taquilla de
                                    cualquier terminal o punto de venta, en las rutas de las marcas participantes, en el
                                    sitio web www.ado.com.mx o ADO Móvil para las rutas de las marcas participantes.
                                </li>
                                <li>Descuento del 15%: El ganador recibirá vía correo electrónico un código alfanumérico
                                    y podrá canjear dicho descuento, durante la vigencia establecida en la taquilla de
                                    cualquier terminal o punto de venta, en las rutas de las marcas participantes, en el
                                    sitio web www.ado.com.mx o ADO Móvil para las rutas de las marcas participantes.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon11">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                            CUANDO ME REGISTRO EN EL SITIO DE LA PROMOCIÓN ME DICE QUE MI NÚMERO DE FOLIO NO ES VÁLIDO
                            ¿POR QUÉ?
                        </button>
                    </h2>
                    <div id="collapse11" class="accordion-collapse collapse" aria-labelledby="heading11"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>Seguramente es porque no has abordado la unidad o concluido tu viaje. Necesitas haber,
                                abordado la unidad o concluido tu viaje y después volver a intentar tu registro. Es
                                importante que tu viaje no haya sido cancelado, haya concluido y sea un boleto adquirido
                                de cualquiera de las marcas de autobuses participantes. Si ya abordaste la unidad o
                                concluyó tu viaje, espera unos momentos y vuelve a intentarlo. Tu boleto debió de ser
                                comprado después de las 00:01 del 04 de octubre al 15 de diciembre del 2022, fecha en la
                                que inició la “PROMOCIÓN”.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon12">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
                            ¿TENGO QUE ENTREGAR MI BOLETO CUANDO CANJEO MI PREMIO?
                        </button>
                    </h2>
                    <div id="collapse12" class="accordion-collapse collapse" aria-labelledby="heading12"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>No. Sin embargo, tu boleto es tu comprobante como participante para poder acreditar que
                                efectivamente eres el ganador o posible ganador del premio correspondiente así como para
                                poder hacerte entrega del mismo. Es importante conservar tu boleto registrado, ya que en
                                caso de ser necesario se te solicitará para su validación. En ningún caso dicho boleto
                                será retenido, únicamente será para efectos de validación del mismo.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon13">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
                            SI ADQUIERO MIS BOLETOS CON COMPRA ANTICIPADA ¿PUEDO PARTICIPAR EN LA PROMOCIÓN?
                        </button>
                    </h2>
                    <div id="collapse13" class="accordion-collapse collapse" aria-labelledby="heading13"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>Todos los boletos que se adquieran durante la vigencia de la promoción serán válidos para
                                poder ganar uno de los premios. ES IMPORTANTE DESTACAR QUE PARA PODER PARTICIPAR EL
                                BOLETO DEBE TENER EL NOMBRE COMPLETO DEL PASAJERO Y ÉSTE DEBE COINCIDIR CON SU
                                IDENTIFICACIÓN OFICIAL.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon14">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                            ¿LAS PERSONAS QUE GANEN DESCUENTOS PODRÁN COMBINARLOS CON OTRAS PROMOCIONES?
                        </button>
                    </h2>
                    <div id="collapse14" class="accordion-collapse collapse" aria-labelledby="heading14"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>Los descuentos otorgados ÚNICAMENTE serán válidos en el precio final del boleto normal o
                                Compra Anticipada. No es acumulable con descuentos INAPAM, niño, Fuerzas Armadas,
                                estudiantes, maestros o boletos abiertos u otro tipo de descuentos. Además, para
                                hacerlos válidos no se aceptan las siguientes formas de pago: orden de servicio,
                                TURISSSTE, cupón Kidde, cupón COTEMAR, pases de traslados, pases de vacaciones, cupón
                                Interjet, MAYAPASS, cupón (USA) y cupón efectivo.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon15">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
                            ¿PARTICIPAN TODOS LOS USUARIOS DE ADO?

                        </button>
                    </h2>
                    <div id="collapse15" class="accordion-collapse collapse" aria-labelledby="heading15"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <p>Para participar se debe ser mayor de 18 años a partir del inicio de la promoción, contar
                                con identificación oficial vigente y otorgar datos verídicos para poderles contactar en
                                caso de que resulten ganadores de alguno de los premios.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="acordeon16">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
                            ¿CUÁLES BOLETOS NO PARTICIPAN PARA EL CANJE DE LOS DESCUENTOS DEL 10% Y 15%?
                        </button>
                    </h2>
                    <div id="collapse16" class="accordion-collapse collapse" aria-labelledby="heading16"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <ul>
                                <li>Boletos que no sean de las marcas participantes.</li>
                                <li>Boletos adquiridos fuera de la vigencia de la promoción.</li>
                                <li>Boletos adquiridos que sean INAPAM, niños, Fuerzas Armadas, pasajero quedado,
                                    estudiantes, maestros y boletos abiertos. Todos los boletos deben de ser enteros, es
                                    decir no haber entrado en ningún tipo de descuento.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="contactanos-links row justify-content-center text-center">
                <div class="col-md-3 pb-4">
                    ¿Necesitas ayuda?
                    <a class="red" href="#"> Contáctanos</a>
                </div>
                <div class="col-md-3 pb-4">
                    Ciudad de México
                    <a href="tel:5784 4652"> 5784 4652</a>
                </div>
                <div class="col-md-3 pb-4">
                    Interior de la República
                    <a href="tel:0155 5784 4652"> 0155 5784 4652</a>
                </div>
            </div>
        </div>
    </div>
    <div class="aficionados">

    </div>
</div>
<component :is="'script'">
window.zESettings = {
    webWidget: {
        color: { theme: '#c8142c' },
         chat: {
            departments: {
                enabled: [],
                select: 'Qatar'
            }
        }
    }
};
</component>
@endsection