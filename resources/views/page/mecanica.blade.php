@extends('layouts.app')
@section('title')
Mecánica
@endsection
@section('content')
<div class="mecanica" id="intro">
    <div class="intro">
        <div class="container">
            <div class="ultimo-registro">
                <strong>
                    @if(isset($ultimo))
                    Último registro: {{str_pad($ultimo->premio->id, 8, '0',STR_PAD_LEFT)}} {{$ultimo->fecha}} hrs.
                    @endif
                </strong>
                <div>
                    Este <span id="fecha_actual"></span> a las <span id="hora_actual"></span> seguro ganas.
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-10">

                    <img class="img-fluid" src="{{asset('img/qatar_intro.png')}}" alt="">
                    <div class="text-center logo-mexico">
                        <img class="img-fluid" src="{{asset('img/seleccion.png')}}" alt="">
                        <h4 class="mt-4">El Autobús Oficial de la Selección Nacional de México</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="info-mecanica">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="mt-4 mt-lg-0 pt-4 pt-lg-0">
                        MECÁNICA
                    </h2>
                    <div class="container">
                        <div class="row row-cols-1 row-cols-lg-4 items">
                            <div class="col">
                                <div>
                                    <img class="img-fluid" src="{{asset('img/mecanica/1.png')}}" alt="Paso 1">
                                </div>
                                <p class="text-center text-md-center">
                                    <span>1. </span>Viaja a tu destino favorito.
                                </p>
                            </div>
                            <div class="col">
                                <div>
                                    <img class="img-fluid" src="{{asset('img/mecanica/2.png')}}" alt="Paso 2">
                                </div>
                                <p class="text-center text-md-center">
                                    <span>2. </span>Registra tu boleto.
                                </p>
                            </div>
                            <div class="col">
                                <div>
                                    <img class="img-fluid" src="{{asset('img/mecanica/3.png')}}" alt="Paso 3">
                                </div>
                                <p class="text-center text-md-center"><span>3. </span>Gana uno de los miles de premios
                                    que tenemos para ti y recibe un
                                    código en
                                    tu
                                    correo para canjearlo.</p>
                            </div>
                            <div class="col">
                                <div>
                                    <img class="img-fluid" src="{{asset('img/mecanica/4.png')}}" alt="Paso 4">
                                </div>
                                <p class="text-center text-md-center"><span>4. </span>¡Sigue participando! </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="increibles">
        <div class="container">
            <h2 class="mb-4 pb-4">
                ¡GANA INCREÍBLES PREMIOS!
            </h2>
            <div class="row justify-content-center mb-4 pb-4">
                <div class="col-8 col-md-6 col-lg-5 col-xl-4">
                    <div class="text-center">
                        <img src="{{asset('img/increibles/premios.png')}}" class="img-fluid" alt="Premios">
                    </div>

                    <div class="text-center">
                        <img src="{{asset('img/increibles/logos-ado.png')}}" class="img-fluid" alt="Premios">
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-7">
                    <div class="text-center">
                        <a href="{{ route('bases') }}" class="btn btn-lg btn-danger">Bases</a>
                        <p class="mt-4 pt-4">
                            Promoción válida del <b>04 de octubre</b> al <b>15 de diciembre</b> del <b> 2022</b> .
                            Viaja del <b> 04 de octubre</b> al <b> 15 de diciembre</b> de <b> 2022</b> .
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="aficionados"></div>
</div>

<component :is="'script'">
    var now = new Date('{{date(DATE_ATOM)}}') * 1;
    var segundos = 0;
    function actualizarFecha(){
        const meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre',
        'noviembre', 'diciembre'];
        const dias_semana = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
        const fecha = new Date(now + (segundos * 1000));
        document.getElementById('fecha_actual').innerText = dias_semana[fecha.getDay()] + ' ' + fecha.getDate() + ' de ' +
        meses[fecha.getMonth()] + ' de ' + fecha.getUTCFullYear();
        document.getElementById('hora_actual').innerText = fecha.getHours().toString().padStart(2, 0) + ':' + fecha.getMinutes().toString().padStart(2, 0)+':'+fecha.getSeconds().toString().padStart(2, 0);
        segundos += 1;
    }
    actualizarFecha();
    setInterval(actualizarFecha, 1000);
</component>
@endsection