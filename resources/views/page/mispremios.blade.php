@extends('layouts.app')
@section('title')
Centros de canje
@endsection
@section('content')
<div class="centros">
    <div class="wrapper pt-4">
        <div class="container pb-4 mb-4">
            <h2 class="titulo-balon">Mis premios ganados</h2>
            <div class="pt-3">
                <div class="table-responsive">
                    <table class="table table-bordered border-primary">
                        <thead class="table-primary text-center">
                            <tr>
                                <th>FOLIO</th>
                                <th>FECHA</th>
                                <th colspan="2">PREMIO</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($premios as $premio)
                            <tr>
                                <td class="text-center">{{$premio->folio->codigo}}</td>
                                <td class="text-justify">{{$premio->folio->created_at}}</td>
                                <td class="text-justify">{{$premio->tipo->titulo}}</td>
                                <td class="text-justify" style="cursor: pointer" data-bs-toggle="modal"
                                    data-bs-target="#premio-{{$premio->folio->codigo}}">DETALLES</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    @foreach($premios as $premio)
    <div style="color: #000;" class="modal fade" id="premio-{{$premio->folio->codigo}}" tabindex="-1"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <h3 style="margin-bottom: -30px: position: relative;z-index: 1;">{{$premio->codigo}}</h3>
                    </div>
                    @if($premio->premio_tipo_id == 1)
                    <div>
                        <img class="img-fluid" src="{{asset('/img/mails/1/index_02.gif')}}">
                    </div>
                    <table style="padding: 0 50px;">
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;"> Presenta el correo electrónico
                                impreso que contiene tu
                                código alfanumérico, original y copia de tu identificación oficial vigente con
                                fotografía por ambos lados y
                                el boleto en original con el cual registraste tu participación en buen estado; es decir,
                                sin tachaduras, ni
                                enmendaduras y/o roto. (Consulta tu centro de canje, horarios y días establecidos) <a
                                    style="color: red; text-decoration: none; font-weight: bold;"
                                    href="https://elautobusmundialista.com.mx/centros-de-canje">HAZ CLICK
                                    AQUÍ.</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;">Cada código alfanumérico es único y
                                personal, no es
                                transferible a otra persona y no podrá usarse en más de una ocasión.
                            </td>
                        </tr>
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;">Aplica para un viaje sencillo de
                                autobús en el periodo de
                                vigencia de la promoción, en las marcas y rutas participantes <a
                                    style="color: red; text-decoration: none; font-weight: bold;"
                                    href="https://elautobusmundialista.com.mx/bases">(Conoce las bases)</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;">Los códigos alfanuméricos ganadores,
                                no son acumulables a
                                otros descuentos y promociones, a saber de manera enunciativa: INAPAM, ESTUDIANTE,
                                MAESTRO; no son
                                reembolsables en efectivo.
                            </td>
                        </tr>
                    </table>
                    @elseif($premio->premio_tipo_id == 2)
                    <div>
                        <img class="img-fluid" src="{{asset('/img/mails/2/index_02.jpg')}}">
                    </div>
                    <table style="padding: 0 50px;">
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;"> Presenta el correo electrónico
                                impreso que contiene tu
                                código alfanumérico, original y copia de tu identificación oficial vigente con
                                fotografía por ambos lados y
                                el boleto en original con el cual registraste tu participación en buen estado; es decir,
                                sin tachaduras, ni
                                enmendaduras y/o roto. (Consulta tu centro de canje, horarios y días establecidos) <a
                                    style="color: red; text-decoration: none; font-weight: bold;"
                                    href="https://elautobusmundialista.com.mx/centros-de-canje">HAZ CLICK
                                    AQUÍ.</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;">Cada código alfanumérico es único y
                                personal, no es
                                transferible a otra persona y no podrá usarse en más de una ocasión.
                            </td>
                        </tr>
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;">Aplica para un viaje sencillo de
                                autobús en el periodo de
                                vigencia de la promoción, en las marcas y rutas participantes <a
                                    style="color: red; text-decoration: none; font-weight: bold;"
                                    href="https://elautobusmundialista.com.mx/bases">(Conoce las bases)</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;">Los códigos alfanuméricos ganadores,
                                no son acumulables a
                                otros descuentos y promociones, a saber de manera enunciativa: INAPAM, ESTUDIANTE,
                                MAESTRO; no son
                                reembolsables en efectivo.
                            </td>
                        </tr>
                    </table>
                    @elseif($premio->premio_tipo_id == 3)
                    <div>
                        <img class="img-fluid" src="{{asset('/img/mails/3/index_02.jpg')}}">
                    </div>
                    <table style="padding: 0 50px;">
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;"> Presenta el correo electrónico
                                impreso que contiene tu
                                código alfanumérico, original y copia de tu identificación oficial vigente con
                                fotografía por ambos lados, copia de tu RFC con homoclave y
                                el boleto en original con el cual registraste tu participación en buen estado; es decir,
                                sin tachaduras, ni
                                enmendaduras y/o roto. (Consulta tu centro de canje, horarios y días establecidos) <a
                                    style="color: red; text-decoration: none; font-weight: bold;"
                                    href="https://elautobusmundialista.com.mx/centros-de-canje">HAZ CLICK
                                    AQUÍ.</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;">Cada código alfanumérico es único y
                                personal, no es
                                transferible a otra persona y no podrá usarse en más de una ocasión.
                            </td>
                        </tr>
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;">Los códigos alfanuméricos ganadores,
                                no son acumulables a
                                otros descuentos y promociones, a saber de manera enunciativa: INAPAM, ESTUDIANTE,
                                MAESTRO; no son
                                reembolsables en efectivo.
                            </td>
                        </tr>
                    </table>

                    @elseif($premio->premio_tipo_id == 4)
                    <div>
                        <img class="img-fluid" src="{{asset('/img/mails/4/index_02.jpg')}}">
                    </div>
                    <table style="padding: 0 50px;">
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;"> Presenta el correo electrónico
                                impreso que contiene tu código alfanumérico,
                                original y copia de tu identificación oficial vigente con fotografía por ambos lados,
                                copia de tu RFC con homoclave y el boleto en original con el cual registraste tu
                                participación en buen estado; es decir, sin tachaduras, ni enmendaduras y/o roto.
                                (Consulta tu centro de canje, horarios y días establecidos) <a
                                    style="color: red; text-decoration: none; font-weight: bold;"
                                    href="https://elautobusmundialista.com.mx/centros-de-canje">HAZ CLICK
                                    AQUÍ.</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;">Cada código alfanumérico es único y
                                personal, no es
                                transferible a otra persona y no podrá usarse en más de una ocasión.
                            </td>
                        </tr>
                        <tr>
                            <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
                            <td style="vertical-align: top; padding-bottom: 20px;">Los códigos alfanuméricos ganadores,
                                no son acumulables a
                                otros descuentos y promociones, a saber de manera enunciativa: INAPAM, ESTUDIANTE,
                                MAESTRO; no son
                                reembolsables en efectivo.
                            </td>
                        </tr>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <div class="aficionados">

    </div>
</div>
@endsection