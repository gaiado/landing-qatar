@extends('layouts.app')
@section('title')
Centros de canje
@endsection
@section('content')
<div class="centros">
    <div class="wrapper pt-4">
        <div class="container pb-4 mb-4">
            <h2 class="titulo-balon">Centros de Canje</h2>
            <div class="pt-3">
                <div class="table-responsive">
                    @include('page.tabla')
                </div>
            </div>
        </div>
    </div>
    <div class="aficionados">

    </div>
</div>
@endsection