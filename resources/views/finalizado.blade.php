@extends('layouts.app')
@section('title')
Inicio
@endsection
@section('content')

<div class="mecanica" id="intro">
    <div class="intro">
        <div class="container">
            <div class="ultimo-registro">
                <strong>
                    @if(isset($ultimo))
                    Último registro: {{str_pad($ultimo->premio->id, 8, '0',STR_PAD_LEFT)}} {{$ultimo->fecha}} hrs.
                    @endif
                </strong>
                <div>
                    Este <span id="fecha_actual"></span> a las <span id="hora_actual"></span> seguro ganas.
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <img class="img-fluid" src="{{asset('img/banner.png?v=2')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<component :is="'script'">
    var now = new Date('{{date(DATE_ATOM)}}') * 1;
    var segundos = 0;
    function actualizarFecha(){
        const meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre',
        'noviembre', 'diciembre'];
        const dias_semana = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
        const fecha = new Date(now + (segundos * 1000));
        document.getElementById('fecha_actual').innerText = dias_semana[fecha.getDay()] + ' ' + fecha.getDate() + ' de ' +
        meses[fecha.getMonth()] + ' de ' + fecha.getUTCFullYear();
        document.getElementById('hora_actual').innerText = fecha.getHours().toString().padStart(2, 0) + ':' + fecha.getMinutes().toString().padStart(2, 0)+':'+fecha.getSeconds().toString().padStart(2, 0);
        segundos += 1;
    }
    actualizarFecha();
    setInterval(actualizarFecha, 1000);
</component>
@endsection