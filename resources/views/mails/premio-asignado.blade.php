<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>El Autobús Mundialista</title>
</head>

<body style="font-size: 18px; text-align: center;padding-top: 50px;box-sizing: border-box;">
    <center>
        <div style="width: 850px;display: inline-block;text-align: left;">
            <div style="text-align: center;font-size: 32px;z-index: 1;position: relative;">
               Código: {{$premio->codigo}}
            </div>
            <div>
                @yield('content1')
            </div>
            <div style="text-align: center;font-size: 32px;margin-bottom: -50px;z-index: 1;position: relative;">
                <br>
                Código: {{$premio->codigo}}
                <br>
             </div>
            <a style="display: block">
                <br>
                <img src="{{$message->embed(public_path().'/img/mails/1/index_03.gif')}}">
        </div>
        <a href="https://play.google.com/store/apps/details?id=com.mobilityado.ado&hl=es_MX" style="display: block">
            <img src="{{$message->embed(public_path().'/img/mails/1/index_04.gif')}}">
        </a>
        <div style="width: 850px">
            <div style="padding: 50px 100px; display: inline-block; background-color: #133d4a;color: #fff;">
                <table style="width: 100%;">
                    <tr>
                        <td
                            style="width: 250px;text-align: right;padding-right: 50px;font-size: 24px;font-weight: bold;">
                            VISÍTANOS</td>
                        <td style="width: 60px">
                            <a style="text-decoration: none;color: inherit;" href="https://www.facebook.com/TuADO/"><img
                                    src="https://www.ado.com.mx/images/facebook.png" alt=""></a>
                        </td>
                        <td style="width: 60px">
                            <a style="text-decoration: none;color: inherit;" href="https://twitter.com/tuado/"><img
                                    src="https://www.ado.com.mx/images/twitter.png" alt=""></a>
                        </td>
                        <td style="width: 60px">
                            <a style="text-decoration: none;color: inherit;"
                                href="https://www.instagram.com/tuadomx/"><img
                                    src="https://www.ado.com.mx/images/instagram.png" alt=""></a>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <p>La información contenida en este mensaje es confidencial y privada, está protegida por secreto
                    profesional y
                    está destinada únicamente para el uso de la(s) persona(s) arriba indicada(s). Está estrictamente
                    prohibida
                    cualquier difusión, distribución, impresión, edición no autorizada, o copia de este mensaje. Si ha
                    recibido
                    esta comunicación o copia de este mensaje por error, favor de desecharlo permanentemente o
                    destruirlo
                    inmediatamente. </p>
                <p>¿No solicitaste este correo? Omite este mensaje. </p>
            </div>
            <div style="color: #fff;padding: 40px 50px;background-color: #ee3030;display: inline-block; ">
                <table style="width: 750px;">
                    <tr>
                        <td><a style="color: inherit;text-decoration: none"
                                href="https://www.ado.com.mx/aviso-de-privacidad"> Aviso de Privacidad</a></td>
                        <td>|</td>
                        <td><a style="color: inherit;text-decoration: none"
                                href="https://www.ado.com.mx/terminos-y-condiciones">Términos y Condiciones</a></td>
                        <td>|</td>
                        <td><a style="color: inherit;text-decoration: none" href="https://hola.ado.com.mx/hc/es">Visita
                                nuestro Centro de ayuda</a></td>
                        <td>|</td>
                        <td><a style="color: inherit;text-decoration: none"
                                href="https://www.ado.com.mx/">www.ado.com.mx</a></td>
                    </tr>
                </table>
            </div>
        </div>
    </center>
</body>

</html>