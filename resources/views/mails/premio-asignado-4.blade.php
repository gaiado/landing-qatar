@extends('mails.premio-asignado')

@section('content1')
<div>
    <img src="{{$message->embed(public_path().'/img/mails/4/index_02.jpg')}}">
</div>
<table style="padding: 0 50px;">
    <tr>
        <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
        <td style="vertical-align: top; padding-bottom: 20px;"> Presenta el correo electrónico impreso que contiene tu código alfanumérico,
original y copia de tu identificación oficial vigente con fotografía por ambos lados,
copia de tu RFC con homoclave y el boleto en original con el cual registraste tu
participación en buen estado; es decir, sin tachaduras, ni enmendaduras y/o roto. (Consulta tu centro de canje, horarios y días establecidos) <a style="color: red; text-decoration: none; font-weight: bold;" href="https://elautobusmundialista.com.mx/centros-de-canje">HAZ CLICK
                AQUÍ.</a>
        </td>
    </tr>
    <tr>
        <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
        <td style="vertical-align: top; padding-bottom: 20px;">Cada código alfanumérico es único y personal, no es
            transferible a otra persona y no podrá usarse en más de una ocasión.
        </td>
    </tr>
    <tr>
        <td style="color:red; vertical-align: top;padding-right: 10px;">&#9670</td>
        <td style="vertical-align: top; padding-bottom: 20px;">Los códigos alfanuméricos ganadores, no son acumulables a
            otros descuentos y promociones, a saber de manera enunciativa: INAPAM, ESTUDIANTE, MAESTRO; no son
            reembolsables en efectivo.
        </td>
    </tr>
</table>

@endsection