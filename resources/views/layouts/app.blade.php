<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>
    <meta name="title" content="ADO te premia por viajar / Viaja, Registra y Gana">
    <meta name="description"
        content="Viaja a tu destino favorito. Registra tu boleto. Gana uno de los miles de premios que tenemos para ti, balones, playeras de la Selección Nacional y descuentos ">

    <!-- Open Graph / Facebook -->
    <meta name="facebook-domain-verification" content="tormbun4rwxvfsannla6180cp68hou" />
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://elautobusmundialista.com.mx/">
    <meta property="og:title" content="ADO te premia por viajar / Viaja, Registra y Gana">
    <meta property="og:description"
        content="Viaja a tu destino favorito. Registra tu boleto. Gana uno de los miles de premios que tenemos para ti, balones, playeras de la Selección Nacional y descuentos ">
    <meta property="og:image" content="Aquí la imagen que te adjunto">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://elautobusmundialista.com.mx/">
    <meta property="twitter:title" content="ADO te premia por viajar / Viaja, Registra y Gana">
    <meta property="twitter:description"
        content="Viaja a tu destino favorito. Registra tu boleto. Gana uno de los miles de premios que tenemos para ti, balones, playeras de la Selección Nacional y descuentos ">
    <meta property="twitter:image" content="">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5GMZZV4');</script>
    <!-- End Google Tag Manager -->

<body>
    </head>

    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="ns" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="app">
            <div class="bg-luz">
                <div class="bg"></div>
                <div class="star-field">
                    <div class="layer"></div>
                    <div class="layer"></div>
                    <div class="layer"></div>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-dark menu-qatar fixed-top ">
                <div class="container">
                    <a href="{{url('')}}"><img id="img-logo" src="{{ asset('img/logo_ado.png') }}" alt="ADO"></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                   
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <?php if(0): ?>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('mecanica') ? 'active' : '' }}"
                                    href="{{ route('mecanica') }}">MECÁNICA</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('premios') ? 'active' : '' }}"
                                    href="{{ route('premios') }}">PREMIOS</a>
                            </li>
                            
                    <?php endif; ?>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('bases') ? 'active' : '' }}"
                                    href="{{ route('bases') }}">BASES</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('centros') ? 'active' : '' }}"
                                    href="{{ route('centros') }}">CENTROS DE CANJE</a>
                            </li>
                            <?php if(0): ?>
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('preguntas') ? 'active' : '' }}"
                                    href="{{ route('preguntas') }}">PREGUNTAS FRECUENTES</a>
                            </li>
                            @guest
                            @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('login') ? 'active' : ''}}"
                                    href="{{ route('login') }}">INICIAR
                                    SESIÓN</a>
                                </a>
                            </li>
                            @endif

                            @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('register') ? 'active' : ''}}"
                                    href="{{ route('register') }}">REGÍSTRATE</a>
                            </li>
                            @endif
                            @else
                            <li class="nav-item">
                                <a class="nav-link {{ Route::is('mispremios') ? 'active' : ''}}"
                                    href="{{ route('mispremios') }}">MIS PREMIOS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">CERRAR
                                    SESIÓN</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                            @endguest
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </nav>

            <main>
                @yield('content')
            </main>
            <footer>
                <div class="footer-gris">
                    <div class="container">
                        <div class="row justify-content-around">
                            <div class="col-auto">
                                <ul class="list-unstyled list-links">
                                    <li><a href="#"><label>REDES SOCIALES</label></a></li>
                                    <li>
                                        <div class="row">
                                            <div class="col-auto">
                                                <a target="_blank" href="https://www.facebook.com/TuADO/"><img
                                                        src="https://www.ado.com.mx/images/facebook.png"
                                                        alt="Facebook"></a>
                                            </div>
                                            <div class="col-auto">
                                                <a target="_blank" href="https://twitter.com/tuado/"><img
                                                        src="https://www.ado.com.mx/images/twitter.png"
                                                        alt="Twitter"></a>
                                            </div>
                                            <div class="col-auto">
                                                <a target="_blank" href="https://www.instagram.com/tuadomx/"><img
                                                        src="https://www.ado.com.mx/images/instagram.png"
                                                        alt="Instagram"></a>
                                            </div>
                                            <div class="col-auto">
                                                <a target="_blank" href="https://www.youtube.com/user/autobusesADO"><img
                                                        src="https://www.ado.com.mx/images/youtube.png"
                                                        alt="YouTube"></a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-auto">
                                <ul class="list-unstyled">
                                    <li><a href="#"><label>ATENCIÓN A CLIENTES</label></a></li>
                                    <li><a target="_blank" href="https://hola.ado.com.mx/"><img
                                                src="https://www.ado.com.mx/images/hola.png" alt="HOLA"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>

</html>