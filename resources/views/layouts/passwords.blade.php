@extends('layouts.app')

@section('content')
<div class="passwords">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-xl-7">
            @yield('content-passwords')
        </div>
    </div>
</div>
</div>
@endsection
