@extends('layouts.app')
@section('title')
Premios
@endsection
@section('content')
<div class="premios">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-6">
                <div class="informacion">
                    <div class="text-center">
                        <img class="img-fluid" src="{{asset('img/banner1.png')}}" alt="Incríbles Premios">
                    </div>

                    <p class="pt-4">
                        Para ganar, realiza los siguientes pasos:
                    </p>
                    <p>
                    <ol class="ps-3">
                        <li>
                            Ingresa tus datos al formulario.
                        </li>
                        <li>Espera mientras validamos la información.</li>
                        <li>¡Felicidades, ya ganaste! Revisa tu correo electrónico y confirma tu premio.</li>
                        <li>Recuerda que tienes hasta el 15 de diciembre del 2022 para canjear tu premio en los
                            centros de
                            canje
                            y/o descuento directamente en taquillas.</li>
                    </ol>
                    </p>
                    <p class="pt-4">
                        Promoción válida del 04 de octubre al 15 de diciembre del 2022.
                        Viaja del 04 de octubre al 15 de diciembre de 2022
                    </p>
                </div>
            </div>
            <div class="col-lg-5">
                
                @yield('content-premios')
            </div>
        </div>
    </div>
</div>
@endsection