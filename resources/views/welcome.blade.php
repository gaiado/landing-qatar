@extends('layouts.app')
@section('title')
Inicio
@endsection
@section('content')
<div class="inicio">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-9">
                <div class="contenedor-inicio">
                    <h1>Viaja, registra y gana</h1>
                    <div class="balon"></div>
                    <img class="img-fluid" src="{{asset('img/cabezaregistro.png')}}" alt="">
                    <div>
                        <a href="{{ route('premios') }}" class="btn btn-success mb-4">Ingresa los datos de tu
                            boleto</a>
                        <p>Promoción válida del <b>04 de octubre</b> al <b>15 de diciembre</b> del <b>
                                2022</b> . Viaja del <b> 03 de
                                octubre</b>
                            al <b> 15 de diciembre</b> de <b> 2022</b> .</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection